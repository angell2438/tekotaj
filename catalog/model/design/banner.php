<?php
class ModelDesignBanner extends Model {
    public function getBanner($banner_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner_image bi LEFT JOIN " . DB_PREFIX . "banner_image_description bid ON (bi.banner_image_id  = bid.banner_image_id) WHERE bi.banner_id = '" . (int)$banner_id . "' AND bid.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY bi.sort_order ASC");

        return $query->rows;
    }
    public function getPhotoFromBanner($data){
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "banner_image bi LEFT JOIN " . DB_PREFIX . "banner_image_description bid ON (bi.banner_image_id  = bid.banner_image_id) WHERE bi.banner_id = '" . (int)$data['banner_id'] . "' AND bid.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY bi.sort_order ASC LIMIT " . (int)$data['start'] . "," . (int)$data['limit']);

        return$query->rows;
    }
    public function getCountPhotoFromBanner($data){
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "banner_image WHERE banner_id = '" . (int)$data['banner_id']. "'");

        return $query->row['total'];
    }
}