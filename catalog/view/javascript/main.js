function searchwidth() {
    var ml = $('.flex-wrapper.flex-right').width();
    aw = ml + -60;
    $('#search .search_block_content').css("width", aw);
    $('#search .search_block_content').css("left", -aw);
}

// функция узнает размер окна браузера, и задает её для блока div
function fullHeight(){
    $('.error-wrapper').css({
        height: $(window).height() + 'px'
    });
}

var h_hght = 145; // высота шапки
var h_mrg = 0;    // отступ когда шапка уже не видна

$(function(){

    var elem = $('header .mini-position');
    var top = $(this).scrollTop();

    if(top > h_hght){

        elem.css('top', h_mrg);
    }
    if ($(window).width() <= 767) {
        $(window).scroll(function(){
            top = $(this).scrollTop();

            if (top+h_mrg < h_hght) {
                elem.removeClass('fixed');
                elem.css('top', (h_hght-top));
            } else {
                elem.addClass('fixed');
                elem.css('top', h_mrg);
            }
        });
    }

});
function get_timer2() {
    $('.timer').each(function () {
        timetext = $(this).data('timetext');
        timeend = $(this).data('timeend');
        var date_t = new Date(timeend);
        var date = new Date();
        var timer = date_t - date;
        if (date_t > date) {
            var day = parseInt(timer / (60 * 60 * 1000 * 24));
            if (day < 10) {
                day = '0' + day;
            }
            day = day.toString();
            var hour = parseInt(timer / (60 * 60 * 1000)) % 24;
            if (hour < 10) {
                hour = '0' + hour;
            }
            hour = hour.toString();
            var min = parseInt(timer / (1000 * 60)) % 60;
            if (min < 10) {
                min = '0' + min;
            }
            min = min.toString();
            var sec = parseInt(timer / 1000) % 60;
            if (sec < 10) {
                sec = '0' + sec;
            }
            sec = sec.toString();
            if (day == 0) {
                var timestr = '<span class="t-day" >' + day + ':</span><span class="t-hour">' + hour + ':</span><span class="t-min">' + min + ':</span><span class="t-sec">' + sec + '</span>';
            } else {
                var timestr = '<span class="t-day" >' + day + ':</span><span class="t-hour">' + hour + ':</span><span class="t-min">' + min + ':</span><span class="t-sec">' + sec + '</span>';
            }
            $(this).html('<span class="banner_timer"> ' + timestr + '</span>');
        } else {
            $(this).html("");
        }
    });
    setTimeout(get_timer2, 1000);
}
$(window).resize( function() {
    fullHeight();
    if ($(window).width() <= 767) {
        $('.js-position-menu').appendTo('.xs-menu-block');
    } else{
        $('.js-position-menu').appendTo('.mini-position');
    }
});
$(window).load( function() {

    $(".ninja-btn").click( function() {
        $(".ninja-btn, .panel-overlay, .panel").toggleClass("active");
        /* Check panel overlay */
        if ($(".panel-overlay").hasClass("active")) {
            $(".panel-overlay").addClass('active-overlay');
        } else {
            $(".panel-overlay").removeClass('active-overlay');
        }
    });

});
$(window).on("load resize", function() {
    var menuHeightOffset = $(".panel").find("ul").height() /2;

    $(".panel").find("ul").css({
        "margin-top": -menuHeightOffset
    });
    $('select.form-control').select2({
        minimumResultsForSearch: Infinity
    });

});

$(document).ready(function () {

    var sld = function() {
        if ($(window).width() <= 1366) {
            $('.category-slider').slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 2,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 1367,
                        settings: {
                            infinite: false,
                            slidesToShow: 5,
                            slidesToScroll: 1,
                            arrows: false
                        }

                    },
                    {
                        breakpoint: 1199,
                        settings: {
                            infinite: false,
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            arrows: false
                        }

                    },
                    {
                        breakpoint: 1024,
                        settings: {
                            infinite: true,
                            slidesToShow: 4,
                            slidesToScroll: 1,
                            arrows: false
                        }

                    },
                    {
                        breakpoint: 992,
                        settings: {
                            infinite: false,
                            slidesToShow: 3,
                            slidesToScroll: 1,
                            arrows: false
                        }

                    },
                    {
                        breakpoint: 768,
                        settings: {
                            infinite: true,
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            arrows: false
                        }

                    },
                    {
                        breakpoint: 680,
                        settings: {
                            infinite: false,
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            arrows: false
                        }

                    },
                    {
                        breakpoint: 550,
                        settings: {
                            infinite: true,
                            slidesToShow: 2,
                            slidesToScroll: 1,
                            arrows: false
                        }

                    },
                    {
                        breakpoint: 480,
                        settings: {
                            infinite: false,
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            arrows: false
                        }

                    }
                ]
            });
        } else {
            // $('.category-slider').slick({
            //     dots: false,
            //     infinite: true,
            //     speed: 300,
            //     slidesToShow: 6,
            //     slidesToScroll: 1
            // });
        }
    };

    $(window).resize(sld);

    function productImageSlider() {
        if ($('*').is('.thumbnails')) {

            $('.thumbnails').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true,
                asNavFor: '.thumbnails-mini'
            });
            $('.thumbnails-mini').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.thumbnails',
                dots: false,
                arrows: false,
                speed: 200,
                centerMode: false,
                focusOnSelect: true,
                responsive: [
                    {
                        breakpoint: 820,
                        settings: {
                            slidesToShow: 4,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
            if ($(window).width() > 767) {
                $('.thumbnails ').on('afterChange', function (event, slick, currentSlide) {
                    $(".thumbnails .slick-current img").elevateZoom({
                        zoomType: "lens",
                        lensShape: "square",
                        lensSize: 160,
                        borderSize: 0
                    });
                });
                $(".thumbnails .slick-current img").elevateZoom({
                    zoomType: "lens",
                    lensShape: "square",
                    lensSize: 160,
                    borderSize: 0
                });
            }
            $('#lightgallery').lightGallery({
                loop: true,
                fourceAutoply: true,
                thumbnail: true,
                hash: false,
                speed: 400,
                scale: 1,
                keypress: true,
                counter: false,
                download: false,
            });
            $('.click-triger-zoom').on('click', function (e) {
                $('.thumbnails .slick-current img').data("link");
                $('#' + $('.thumbnails .slick-current img').data("link")).trigger("click");
            });
        }
    }

    get_timer2();
    fullHeight();
    sld();
    searchwidth();
    productImageSlider();

    if ($(window).width() <= 767) {
        $('.js-position-menu').appendTo('.xs-menu-block');
    } else{
        $('.js-position-menu').appendTo('.mini-position');
    }

    $('button.wishlist-js').click(function () {
        $(this).attr('disabled', true);
    });

    var url = document.location.href;
    $.each($(".color-radio a"), function() {
        if (this.href == url) {
            $(this).parent().addClass('activeCSS');
        }
    });

    $('.js-information-menu').click(function (e) {
        $('body, html').toggleClass('overflow-body');
        $('.top').toggleClass('open');
        $(this).toggleClass('active');
        $('.wrapper-top-menu .list-inline').toggleClass('active');
    });

    $('.js-catalog-menu').click(function (e) {
        $('.navbar').toggleClass('open');
    });
    $(".in_product").fancybox({
        openEffect	: 'elastic',
        closeEffect	: 'elastic',
        helpers : {
            title : {
                type : false
            },
            overlay: {
                locked: false
            }
        }
    });
    $('.slider-wrapper').slick({
        infinite: false,
        slidesToShow: 3,
        arrows: true,
        dots: false,
        dotsClass: 'products_row-dots',
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn compare-btn"><i class="fa fa-angle-left" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn compare-btn"><i class="fa fa-angle-right" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,

                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 460,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });
    $('.mfilter-box .box-heading').click(function () {
        $(this).toggleClass('open');
        $('.mfilter-content').toggleClass('open');
    });
    $(".footer-title").click(function(e){
        e.preventDefault();
        if ($(window).width() <= 768) {
            var $parentBlock = $(this).parent('.parent-elements');
            console.log($parentBlock);
            $parentBlock.toggleClass("open-list");
            if($parentBlock.hasClass("open-list")){
                $(".open-list").find('.list-unstyled').slideDown(500);
            } else {
                $(".list-unstyled").slideUp(500);
            }
            $(this).toggleClass("open");
        } else {
        }
    });

    $('.qty_block .button_plus').click(function () {
        var num = $('.qty_block input[name=quantity]').val();
        var min = parseInt($('.qty_block input[name=quantity]').data('min'));
        $('.qty_block input[name=quantity]').val(parseInt(num)+parseInt($('.qty_block input[name=quantity]').data('min')));
    });
    $('.qty_block .button_minus').click(function () {
        if($('.qty_block input[name=quantity]').val() > $('.qty_block input[name=quantity]').data('min')) {
            var num = $('.qty_block input[name=quantity]').val();
            $('.qty_block input[name=quantity]').val(parseInt(num) - parseInt($('.qty_block input[name=quantity]').data('min')));
        }
    });
    $(".slick-slider").on('init', function(event, slick) {
        $(".slick-slider").css("opacity", "1");
    });

    $('.js-toggle-main-menu').click(function(){
        $(this).toggleClass('active');
    });
    $('.text-share').click(function(){
        $('.share_wrapp').toggleClass('active');
    });
    $(".filter-sm").click(function(e){
        e.preventDefault();
        if ($(window).width() <= 991) {
            $("#column-left").toggleClass("open-filter");
            $(this).toggleClass("open");
        } else {
        }
    });
    $(".wishlist-js").on('click', function(){
        $(this).addClass('active').prop('disabled', true);
        $(this).html('<i class="icon-like-black-heart-button" aria-hidden="true"></i>');
    });

    var url = document.location.href;
    $.each($(".active-menu li.menu-attr a"), function() {
        if (this.href == url) {
            $(this).parent().addClass('activeCSS');
        };
    });


    $('button.in_wishlist').click(function () {
        $(this).attr('disabled', true);
    });

    var countLi = $('.mfilter-content ul li').size();
    if(countLi > 2){
        $('.mfilter-openall a').on('click', function(e){
            e.preventDefault();

            var
                $this = $(this),
                content = $('.mfilter-content ul');

            if(!$this.hasClass('trigger')){
                $this.addClass('trigger');
                $this.html('Скрыть <i class="fa fa-angle-up" aria-hidden="true">');
                content.addClass('open');
            } else {
                $this.removeClass('trigger');
                $this.html('Показать все <i class="fa fa-angle-down" aria-hidden="true">');
                content.removeClass('open');
            }
        });
    } else {
        $('.mfilter-openall').hide();
    }

    $('input[type=tel], input[name="phone"], #customer_telephone, input[name="telephone"]').mask('(999) 999-99-99');

    $('select.form-control').select2({
        minimumResultsForSearch: Infinity
    });

    $("input[name=quantity]").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $("#input-amount").keypress(function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
    $('.js-height').matchHeight({
        property: 'height'
    });
    $('.caption').matchHeight({
        property: 'height'
    });
    $('.artblock').matchHeight({
        property: 'height'
    });
    $('.payment-container').matchHeight({
        property: 'height'
    });
    if ($(window).width() <= 991) {
        $('.text__news .description').dotdotdot({
            height: 80
        });
    } else {

    }
    if ($(window).width() <= 1024) {
        $('.slider-init-sm').not('.slick-initialized').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            arrows: true,
            prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
            nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
            speed: 300,
            centerPadding: '54px',
            centerMode: true,
            responsive: [
                {
                    breakpoint: 767,
                    settings: {
                        infinite: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        centerPadding: '0px',
                        centerMode: false,
                    }

                },
                {
                    breakpoint: 550,
                    settings: {
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        centerPadding: '0px',
                        centerMode: false,
                    }

                },
                {
                    breakpoint: 480,
                    settings: {
                        infinite: false,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                        centerPadding: '0px',
                        centerMode: false,
                    }

                }
            ]
        });
    } else {

    }
    $(".slide_wrapper_big").on('init', function(event, slick) {
        $(".slide_wrapper_big").css("opacity", "1");
    });

    $('.slide_wrapper_big').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
        dots: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    arrows: false
                }

            },
            {
                breakpoint: 620,
                settings: {
                    arrows: false
                }

            }
        ]
    });


    $('.js_mini').not('.slick-initialized').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
        dots: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 620,
                settings: {
                    arrows: false
                }

            }
        ]
    });
    $('.wrapper_news_slider').not('.slick-initialized').slick({
        slidesToShow: 2,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
        dots: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 720,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 590,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }

            }
        ]
    });

    // $('.slick-carousel').not('.slick-initialized').slick({
    //     slidesToShow: 4,
    //     slidesToScroll: 1,
    //     arrows: false,
    //     infinite: false,
    //     dots: false,
    //     prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
    //     nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
    //     initialSlide: 0,
    //     responsive: [
    //         {
    //             breakpoint: 1199,
    //             settings: {
    //                 infinite: false,
    //                 slidesToShow: 4,
    //                 slidesToScroll: 1
    //             }
    //         },
    //         {
    //             breakpoint: 1025,
    //             settings: {
    //                 infinite: true,
    //                 slidesToShow: 3,
    //                 slidesToScroll: 1,
    //                 arrows: false
    //             }
    //         },
    //         {
    //             breakpoint: 990,
    //             settings: {
    //                 infinite: true,
    //                 slidesToShow: 3,
    //                 slidesToScroll: 1,
    //                 arrows: false
    //             }
    //         },
    //         {
    //             breakpoint: 768,
    //             settings: {
    //                 infinite: true,
    //                 slidesToShow: 2,
    //                 slidesToScroll: 1,
    //                 arrows: false,
    //                 dots: false,
    //             }
    //
    //         },
    //         {
    //             breakpoint: 720,
    //             settings: {
    //                 infinite: true,
    //                 slidesToShow: 2,
    //                 slidesToScroll: 1,
    //                 arrows: false
    //             }
    //
    //         },
    //         {
    //             breakpoint: 520,
    //             settings: {
    //                 infinite: true,
    //                 slidesToShow: 2,
    //                 slidesToScroll: 1,
    //                 arrows: false
    //             }
    //
    //         }
    //     ]
    // });

    $('.html_module_slider').not('.slick-initialized').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: false,
        infinite: false,
        dots: false,
         prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    infinite: false,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 990,
                settings: {
                    infinite: false,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    infinite: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true,
                }

            },
            {
                breakpoint: 720,
                settings: {
                    infinite: false,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true,
                }

            },
            {
                breakpoint: 520,
                settings: {
                    infinite: false,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    dots: true,
                }

            }
        ]
    });

    $('.wrapper_prod_slider').not('.slick-initialized').slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
        dots: true,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
        initialSlide: 0,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 1025,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 990,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true,
                    dots: true,
                }

            },
            {
                breakpoint: 720,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 580,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }

            }
        ]
    });
    $('.viewed-list').not('.slick-initialized').slick({
        slidesToShow: 5,
        initialSlide: 0,
        slidesToScroll: 1,
        arrows: true,
        centerMode: true,
        infinite: false,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 990,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 720,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 520,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }

            }
        ]
    });

    $('.viewed-news').not('.slick-initialized').slick({
        slidesToShow: 4,
        initialSlide: 0,
        slidesToScroll: 1,
        arrows: true,
        centerMode: true,
        infinite: false,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 990,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 720,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: false
                }

            },
            {
                breakpoint: 520,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false
                }

            }
        ]
    });

    $('.galery-list').not('.slick-initialized').slick({
        slidesToShow: 5,
        initialSlide: 0,
        slidesToScroll: 1,
        arrows: true,
        infinite: false,
        prevArrow: '<button type="button" class="products_row-prev_btn products_row-btn"><i class="icon-right-chevron-1" aria-hidden="true"></i></button>',
        nextArrow: '<button type="button" class="products_row-next_btn products_row-btn"><i class="icon-right-chevron-1_2" aria-hidden="true"></i></button>',
        responsive: [
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 990,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    arrows: true
                }

            },
            {
                breakpoint: 720,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    arrows: true
                }

            },
            {
                breakpoint: 520,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true
                }

            }
        ]
    });



    $('input, textarea').focus(function () {
        $(this).closest('.has-error').find('.simplecheckout-error-text').remove();
        $(this).closest('.has-error').find('.text-danger').remove();
        $(this).closest('.has-error').removeClass('has-error');
    });

    $('input, textarea').focus(function () {
        $(this).closest('form').find('.alert-danger').remove();
    });

    $('.form-style input').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });
    $('.form-group input').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error ');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });

    $('.form-group textarea').focus(function() {
        $(this).closest('.form-group ').find('.simplecheckout-rule-group').remove();
        $(this).closest('.form-group ').removeClass('has-error ');
        $(this).closest('.form-group ').find('.error').remove();
        $(this).closest('.form-group ').find('.text-danger').remove();
    });
    $(document).on('click', '.btn-simple button', function () {
        $('#'+$(this).data('ids')).trigger('click');
    });
    $('.show_pass_ch input').change(function () {
        if($(this).is(':checked')){
            $(this).parent().parent().parent().parent().find('input[name=password]').attr('type','text');
        }else{
            $(this).parent().parent().parent().parent().find('input[name=password]').attr('type','password');

        }
    });
});
$(document).mouseup(function (e) {
    var container = $(".colorSelector"),
        container2 = $(".colorSelector2");

    if (container.has(e.target).length === 0){
        container.hide();
    }
    if (container2.has(e.target).length === 0){
        container2.hide();
    }
});
