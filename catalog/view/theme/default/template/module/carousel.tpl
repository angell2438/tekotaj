<div class="carousel-wrapper">
    <div class="container">
        <div id="carousel<?php echo $module; ?>" class="slick-carousel">
            <?php foreach ($banners as $banner) { ?>
                <div class="item col-md-3 col-sm-6 col-xs-6 text-center">
                    <?php if ($banner['link']) { ?>
                        <a href="<?php echo $banner['link']; ?>"><img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" /></a>
                    <?php } else { ?>
                        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title']; ?>" class="img-responsive" />
                    <?php } ?>
                </div>
            <?php } ?>
        </div>

    </div>
</div>
