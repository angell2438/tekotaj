<div class="container">
    <div class="row">
        <h3><?php echo $heading_title; ?></h3>
        <div class="container-item_module">
            <div class="wrapper_prod_slider clearfix">
                <?php foreach ($products as $product) { ?>
                    <?php $this->partial('product_item_module', array(
                        'product' => $product,
                        'button_cart' => $button_cart,
                        'text_timer' => $text_timer,
                        'button_wishlist' => $button_wishlist,
                        'button_compare' => $button_compare,
                        'by_one_click' => $by_one_click )); ?>
                <?php } ?>
            </div>
        </div>
    </div>

</div>
