<div class="container-item_module wrapper-testimonial_module">
    <div class="container">
        <h3><?php echo $heading_title; ?></h3>
        <div class="row">
            <div class="wrapper_prod_slider js_mini clearfix">
                <?php foreach ($reviews as $review) { ?>
                    <div class="item">
                            <p class="name-author"><?php echo $review['author']; ?></p>
                            <p class="c-text">
                                <?php echo $review['text']; ?>
                            </p>
                    </div>
                <?php } ?>
            </div>
            <?php if($button_all){ ?>
                <div class="horizontal-sreview-all"><a class="btn-default" href="<?php echo $keyword; ?>"><?php echo $button_all_text; ?></a></div>
            <?php } ?>
        </div>
    </div>
</div>
