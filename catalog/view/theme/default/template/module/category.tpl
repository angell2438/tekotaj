<?php if ($categories) { ?>
    <div class="wrapper-category_home clearfix">
        <div class="container-fluid">
            <div class="slider-init-sm clearfix">
                <?php foreach ($categories as $category) { ?>
                    <div class="item col-md-4">
                        <?php if($category['thumb']) { ?>
                            <img src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name_blue']; ?>">
                        <?php } ?>
                        <div class="category-text">
                            <p class="text_blue"><?php echo $category['name_blue']; ?></p>
                            <p class="text_white"><?php echo $category['name_white']; ?></p>
                            <a class="btn-default" href="<?php echo $category['href']; ?>"><?php echo $text_watch; ?></a>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>