<div id="slideshow<?php echo $module; ?>" class="slide_wrapper_big">
  <?php foreach ($banners as $banner) { ?>
  <div class="item" style='background-image: url(<?php echo $banner['image']; ?>); background-position: center center; background-repeat: no-repeat; background-size: cover;'>
    <?php if ($banner['link']) { ?>
        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title_whith']; ?>" class="img-responsive hidden-sm hidden-xs" />
        <div class="wrapper-banner-info">
            <div class="container">
                <div class="row">
                    <h2>
                        <?php echo $banner['title_whith']; ?>
                        <span><?php echo $banner['text_black']; ?></span>
                    </h2>
                    <p><?php echo $banner['text_mini']; ?></p>
                    <a class="btn-slider" href="<?php echo $banner['link']; ?>"><?php echo $text_watch; ?></a>
                </div>

            </div>

        </div>
    <?php } else { ?>
        <img src="<?php echo $banner['image']; ?>" alt="<?php echo $banner['title_whith']; ?>" class="img-responsive hidden-sm hidden-xs" />
        <div class="wrapper-banner-info">
            <div class="container">
                <div class="row">
                    <h2>
                        <?php echo $banner['title_whith']; ?>
                        <span><?php echo $banner['text_black']; ?></span>
                    </h2>
                    <p><?php echo $banner['text_mini']; ?></p>
                </div>
            </div>

        </div>
    <?php } ?>
  </div>
  <?php } ?>
</div>
