<?php echo $header; ?>
<?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
        <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
<?php } ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>


    <div class="container">

        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">

                <?php echo $content_top; ?>
                <div class="wrapper-margin">
                    <h1 class="hidden"><?php echo $heading_title; ?></h1>
                    <div class="row">
                        <?php if ($products) { ?>
                            <?php foreach ($products as $product) { ?>
                                <div class="product-layout col-lg-3 col-md-3 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="product-thumb transition">
                                        <a class="mini-btn-close" href="<?php echo $product['remove']; ?>" ><i class="icon-close"></i></a>
                                        <div class="image">
                                            <div class="sale-label">
                                                <?php if ($product['sales']) { ?>

                                                    <?php if ($product['count_end_date']) { ?>
                                                        <div class="timer-curier">
                                                            <span class="timar-info"><?php echo  $text_timer; ?></span>
                                                            <div class="timer"
                                                                 data-timeend="<?php echo $product['count_end_date']; ?>"
                                                                 data-timetext="<?php echo str_replace('"','', $product['name']); ?>">

                                                            </div>

                                                        </div>
                                                    <?php } ?>
                                                    <div class="plash top-sales">
                                                        <span>-<?php echo $product['sales']; ?>%</span>
                                                    </div>
                                                <?php } ?>
                                                <?php if ($product['news']) { ?>
                                                    <div class="plash new-prod">
                                                        <span>New</span>
                                                    </div>
                                                <?php } ?>


                                            </div>
                                            <a href="<?php echo $product['href']; ?>">
                                                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                            </a>
                                        </div>
                                        <div class="caption">
                                            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                            <?php if ($product['price']) { ?>
                                                <p class="price">
                                                    <?php if (!$product['special']) { ?>
                                                        <?php echo $product['price']; ?>
                                                    <?php } else { ?>
                                                        <span class="price-new"><?php echo $product['special']; ?></span>
                                                        <span class="price-old"><?php echo $product['price']; ?></span>
                                                    <?php } ?>
                                                </p>
                                            <?php } ?>
                                        </div>

                                        <div class="button-group">

                                            <?php if ($product['compare_status']){ ?>
                                                <button class="mini-btn compare-js active" type="button" > <i class="icon-bars-graphic"></i></button>
                                            <?php } else { ?>
                                                <button class="mini-btn compare-js" type="button"  onclick="compare.add('<?php echo $product['product_id']; ?>');">
                                                    <i class="icon-bars-graphic"></i>
                                                </button>
                                            <?php } ?>
                                            <button class="mini-btn wishlist-js active" type="button" ><i class="icon-like-black-heart-button"></i></button>
                                            <button class="mini-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-shopping-purse-icon-1"></i></button>
                                            <button class="one_click" onclick="get_popup_purchase(<?php echo $product['product_id']; ?>);" data-dismiss="modal"><?php echo $by_one_click; ?></button>
                                        </div>
                                    </div>

                                </div>
                            <?php } ?>
                        <?php } else { ?>
                            <div class="col-xs-12">
                                <p><?php echo $text_empty; ?></p>
                                <div class="buttons clearfix">
                                    <div class=""><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
                                </div>
                            </div>

                        <?php } ?>
                    </div>
                </div>

                <?php echo $content_bottom; ?>


            </div>
            <?php echo $column_right; ?></div>
    </div>
<?php echo $footer; ?>