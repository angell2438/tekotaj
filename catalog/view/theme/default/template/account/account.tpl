<?php echo $header; ?>
<?php if ($success) { ?>
    <div class="user_message_wrapp">
        <div class="container alert alert-success">
            <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
    </div>
<?php } ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>

<div class="container">

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

            <div class="simple-content">
                <div class="account_title">
                    <h1><?php echo $text_account; ?></h1>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <ul class="account_left_bar">
                            <li><a href="<?php echo $link_profil; ?>"><?php echo $text_profil; ?></a></li>
                            <li><a href="<?php echo $link_password; ?>"><?php echo $text_password; ?></a></li>
                            <li><a href="<?php echo $link_history; ?>"><?php echo $text_history; ?></a></li>
                            <li><a href="<?php echo $link_logout; ?>"><?php echo $text_logout; ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>

            <?php echo $content_bottom; ?>
        </div>
        <?php echo $column_right; ?>
    </div>
</div>
<?php echo $footer; ?>
