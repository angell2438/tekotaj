<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <?php echo $content_top; ?>
            <div class="row">
                <h1 class="hidden"><?php echo $heading_title; ?></h1>
                <div class="block-information-contact">
                    <div class="row">
                        <?php foreach ($downloads as $download) { ?>
                            <div class="col-sm-4">
                                <div class="contact-info_big price js-height">
                                    <i class="<?php echo $download['icon']?>"></i>
                                    <span><?php echo $download['category_name']?></span>
                                    <a class="btn-default orang" href="<?php echo $download['download']?>" target="_blank"><span class="icon-download-button"></span><?php echo $download_price; ?></a>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?>
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>
