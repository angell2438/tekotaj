<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <?php echo $content_top; ?>
            <div class="row">
                <h1 class="hidden"><?php echo $heading_title; ?></h1>
                <div class="article-content information-information">
                    <div class="wrapper-voucher price">
                        <h2><?php echo $text_un_login1; ?></h2>
                        <p><?php echo $text_un_login2; ?></p>
                        <a class="btn-default orang" href="" data-toggle="modal" data-target="#modal-authorization" target="_blank"><?php echo $text_authorization; ?></a>
                    </div>
                </div>
            </div>
            <?php echo $content_bottom; ?>
            <?php echo $column_right; ?>
        </div>
    </div>
</div>
<?php echo $footer; ?>
