<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">
  <?php if ($error_warning) { ?>
  <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="wrapper-voucher">
            <h1><?php echo $heading_title; ?></h1>
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
                <div class="row">
                <div class="col-md-6 col-sm-12">

                    <div class="form-group required">
                        <input type="text" name="to_name" value="<?php echo $to_name; ?>" id="input-to-name" class="form-control" placeholder="<?php echo $entry_to_name; ?>"/>
                        <?php if ($error_to_name) { ?>
                            <div class="text-danger"><?php echo $error_to_name; ?></div>
                        <?php } ?>

                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="form-group required">
                        <input type="text" name="to_email" value="<?php echo $to_email; ?>" id="input-to-email" class="form-control" placeholder="<?php echo $entry_to_email; ?>"/>
                        <?php if ($error_to_email) { ?>
                            <div class="text-danger"><?php echo $error_to_email; ?></div>
                        <?php } ?>
                    </div>
                </div>
                </div>
                <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="form-group required">
                        <input type="text" name="from_name" value="<?php echo $from_name; ?>" id="input-from-name" class="form-control" placeholder="<?php echo $entry_from_name; ?>"/>
                        <?php if ($error_from_name) { ?>
                            <div class="text-danger"><?php echo $error_from_name; ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="form-group required">
                        <input type="text" name="from_email" value="<?php echo $from_email; ?>" id="input-from-email" class="form-control" placeholder="<?php echo $entry_from_email; ?>"/>
                        <?php if ($error_from_email) { ?>
                            <div class="text-danger"><?php echo $error_from_email; ?></div>
                        <?php } ?>
                    </div>
                </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="container-inp">
                            <div class="form-group required">
                                <label class="control-label"><?php echo $entry_theme; ?></label>
                                <div class="radio-items ">
                                    <div class="flex-row">
                                        <?php foreach ($voucher_themes as $voucher_theme) { ?>
                                            <?php if ($voucher_theme['voucher_theme_id'] == $voucher_theme_id) { ?>
                                                <div class="radio">
                                                    <label class="form-checkbox">
                                                        <input type="radio" name="voucher_theme_id" value="<?php echo $voucher_theme['voucher_theme_id']; ?>" checked="checked" />
                                                        <span class="form-checkbox__marker"></span>
                                                        <span class="form-checkbox__label">
                                                <?php echo $voucher_theme['name']; ?>
                                            </span>
                                                    </label>
                                                </div>
                                            <?php } else { ?>
                                                <div class="radio">
                                                    <label class="form-checkbox">
                                                        <input type="radio" name="voucher_theme_id" value="<?php echo $voucher_theme['voucher_theme_id']; ?>" />
                                                        <span class="form-checkbox__marker"></span>
                                                        <span class="form-checkbox__label">
                                                <?php echo $voucher_theme['name']; ?>
                                            </span>
                                                    </label>
                                                </div>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>

                                    <?php if ($error_theme) { ?>
                                        <div class="text-danger"><?php echo $error_theme; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <textarea name="message" cols="40" rows="5" id="input-message" class="form-control" placeholder="<?php echo $entry_message; ?>"><?php echo $message; ?></textarea>

                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <input type="text" name="amount" value="" id="input-amount" class="form-control" size="5" placeholder="<?php echo $entry_amount; ?>"/>
                            <?php if ($error_amount) { ?>
                                <div class="text-danger"><?php echo $error_amount; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <div class="buttons clearfix">
                    <div class="form-btn">
                        <?php if ($agree) { ?>
                            <label class="form-checkbox">
                                <input type="checkbox" name="agree" value="1" checked="checked" />
                            <span class="form-checkbox__marker"></span>
                            <span class="form-checkbox__label">
                                               <?php echo $text_agree; ?>
                                            </span>
                            </label>
                        <?php } else { ?>
                            <label class="form-checkbox">
                                <input type="checkbox" name="agree" value="1" />
                                <span class="form-checkbox__marker"></span>
                                <span class="form-checkbox__label">
                                               <?php echo $text_agree; ?>
                                            </span>
                            </label>
                        <?php } ?>
                        &nbsp;
                        <input type="submit" value="<?php echo $button_voucher; ?>" class="btn btn-default orang" />
                    </div>
                </div>
            </form>
        </div>

      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>