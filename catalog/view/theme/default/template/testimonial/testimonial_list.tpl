<?php if ($reviews) { ?>
    <div class="row">
        <?php foreach ($reviews as $review) { ?>
            <div class="col-md-4">
                <div class="c-reviews">
                    <div class="user-image">
                        <i class="icon-user-3"></i>
                    </div>

                    <p class="c-text-m"><?php echo $review['author']; ?></p>
                    <p class="c-text">
                        <?php echo $review['text']; ?>
                    </p>
                    <div class="rating">
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                            <?php if ($review['rating'] < $i) { ?>
                                <span class="fa fa-stack "><i class="fa fa-star"></i></span>
                            <?php } else { ?>
                                <span class="fa fa-stack active"><i class="fa fa-star"></i></span>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>


    <div class="row">
        <div class="col-sm-12"><?php echo $pagination; ?></div>
    </div>
<?php } else { ?>
<p><?php echo $text_no_reviews; ?></p>
<?php } ?>
