<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
    <div class="container">
        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>">
                <?php echo $content_top; ?>
                <div class="row">
                    <h1 class="hidden"><?php echo $heading_title; ?></h1>
                    <div class="tabs-element">
                        <!-- Навигация -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-review-all" data-toggle="tab"><?php echo $text_review_all; ?></a></li>
                            <?php if ($review_status) { ?>
                                <li><a href="#tab-review-prod" data-toggle="tab"><?php echo $text_review_prod; ?></a></li>
                            <?php } ?>
                        </ul>
                        <!-- Содержимое вкладок -->
<!--                        <div class="row">-->
                            <div class="tab-content clearfix">
                                <div role="tabpanel" class="tab-pane active" id="tab-review-all">
                                    <div id="review"></div>
                                    <div class="c-form-style clearfix">

                                        <form class="form-horizontal white-style-form" id="form-review">
                                            <h2><?php echo $text_write; ?></h2>
                                            <?php if ($review_guest) { ?>
                                                <div class="clearfix">
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <input type="text" name="name" value="" id="input-name" class="form-control" placeholder="<?php echo $entry_name; ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <input type="text" name="email" value="" id="input-email" class="form-control" placeholder="<?php echo $entry_email; ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="required">
                                                            <div class="c-reviewStars-input">
                                                                <p class="c-rating"><?php echo$entry_rating; ?></p>
                                                                <div class="reviewStars-input">
                                                                    <input type="radio" name="rating" id="star-5" value="5">
                                                                    <label for="star-5" class="star"> <i class="fa fa-star"></i></label>
                                                                    <input type="radio" name="rating" id="star-4" value="4">
                                                                    <label for="star-4" class="star"> <i class="fa fa-star"></i></label>
                                                                    <input type="radio" name="rating" id="star-3" value="3">
                                                                    <label for="star-3" class="star"> <i class="fa fa-star"></i></label>
                                                                    <input type="radio" name="rating" id="star-2" value="2">
                                                                    <label for="star-2" class="star"> <i class="fa fa-star"></i></label>
                                                                    <input type="radio" name="rating" id="star-1" value="1">
                                                                    <label for="star-1" class="star"> <i class="fa fa-star"></i></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group required">
                                                    <div class="col-sm-12">
                                                        <textarea name="text" rows="5" id="input-review" class="form-control" placeholder="<?php echo $entry_review; ?>"></textarea>
                                                    </div>
                                                </div>
                                                <div class="buttons c-center clearfix">
                                                    <button type="button" id="button-review"
                                                            class="btn btn-default orang"><?php echo $entry_good; ?></button>

                                                </div>
                                            <?php } else { ?>
                                                <?php echo $text_login; ?>
                                            <?php } ?>
                                        </form>
                                    </div>
                                </div>
                                <div class="tab-pane" id="tab-review-prod">
                                    <div id="review-prod"></div>
                                </div>
                            </div>
<!--                        </div>-->
                    </div>
                </div>


                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>

        <script ><!--
            $('#review').delegate('.pagination a', 'click', function (e) {
                e.preventDefault();
                $('#review').load(this.href);
            });

            $('#review').load('<?php echo html_entity_decode($review); ?>');

            $('#review-prod').delegate('.pagination a', 'click', function(e) {
                e.preventDefault();

                $('#review-prod').fadeOut('slow');

                $('#review-prod').load(this.href);

                $('#review-prod').fadeIn('slow');
            });

            $('#review-prod').load('<?php echo html_entity_decode($review_prod); ?>');

            $('#button-review').on('click', function () {
                $.ajax({
                    url: '<?php echo html_entity_decode($write); ?>',
                    type: 'post',
                    dataType: 'json',
                    data:  $("#form-review").serialize(),
                    beforeSend: function () {
                        if ($("textarea").is("#g-recaptcha-response")) {
                            grecaptcha.reset();
                        }
                        $('#button-review').button('loading');
                    },
                    complete: function () {
                        $('#button-review').button('reset');
                    },
                    success: function (json) {
                        $('.alert-success, .alert-danger').remove();
                        if (json['error']) {
                            $('#form-review .buttons').before('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                        }

                        if (json['success']) {
//				$('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');
                            $('#form-review .buttons').before('<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> ' + json['success'] + '</div>');

                            $('input[name=\'name\']').val('');
                            $('textarea[name=\'text\']').val('');
                            $('input[name=\'rating\']:checked').prop('checked', false);
                        }
                    }
                });
            });
            //--></script>
    </div>
<?php echo $footer; ?>