<div class="wrapper-breadcrumb">
    <div class="container">
        <div class="row">
            <div class="title-breadcrumb"><?php echo $breadcrumbs[1]['text']; ?></div>
            <ul class="breadcrumb clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
                                <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                                <i class="icon-right-chevron-1_2"></i>
                            </a>
                        <?php } else { ?>
                            <span itemprop="name"><?php echo $breadcrumb['text']; ?></span>
                        <?php } ?>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>