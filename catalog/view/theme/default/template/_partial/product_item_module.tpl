<div class="product-layout col-lg-3 col-md-4 col-sm-4 col-xs-6 col-xxs-12">
    <div class="product-thumb transition">
        <div class="image">
            <div class="sale-label">
                <?php if ($product['sales']) { ?>

                    <?php if ($product['count_end_date']) { ?>
                        <div class="timer-curier">
                            <span class="timar-info"><?php echo  $text_timer; ?></span>
                            <div class="timer"
                                 data-timeend="<?php echo $product['count_end_date']; ?>"
                                 data-timetext="<?php echo str_replace('"','', $product['name']); ?>">

                            </div>

                        </div>
                    <?php } ?>
                    <div class="plash top-sales">
                        <span>-<?php echo $product['sales']; ?>%</span>
                    </div>
                <?php } ?>
                        <?php if ($product['news']) { ?>
                            <div class="plash new-prod">
                                <span>New</span>
                            </div>
                        <?php } ?>


            </div>


            <a href="<?php echo $product['href']; ?>">
                <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
            </a>
        </div>
        <div class="caption">
            <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
            <?php if ($product['price']) { ?>
                <p class="price">
                    <?php if (!$product['special']) { ?>
                        <?php echo $product['price']; ?>
                    <?php } else { ?>
                        <span class="price-new"><?php echo $product['special']; ?></span>
                        <span class="price-old"><?php echo $product['price']; ?></span>
                    <?php } ?>
                </p>
            <?php } ?>
        </div>
        <div class="button-group">
            <?php if ($product['compare_status']){ ?>
                <button class="mini-btn compare-js active" type="button" > <i class="icon-bars-graphic"></i></button>
            <?php } else { ?>
                <button class="mini-btn compare-js" type="button"  onclick="compare.add('<?php echo $product['product_id']; ?>');">
                    <i class="icon-bars-graphic"></i>
                </button>
            <?php } ?>
            <?php if ($product['wishlist_status']){ ?>
                <button class="mini-btn wishlist-js active" type="button" ><i class="icon-like-black-heart-button"></i></button>
            <?php } else { ?>
                <button class="mini-btn wishlist-js" type="button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                    <i class="icon-like-black-heart-button"></i></button>
            <?php } ?>

            <button class="mini-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-shopping-purse-icon-1"></i></button>
            <button class="one_click" onclick="get_popup_purchase(<?php echo $product['product_id']; ?>);" ><?php echo $by_one_click; ?></button>
        </div>
    </div>
</div>