<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="hidden"><?php echo $heading_title; ?></h1>
      <?php if ($categories) { ?>
          <div class="row">
              <?php foreach ($categories as $category) { ?>
                  <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="brand-block ">
                          <h2 id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></h2>
                          <?php if ($category['manufacturer']) { ?>
                              <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
                                  <div class="name-brand">
                                      <?php foreach ($manufacturers as $manufacturer) { ?>
                                          <a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a>
                                      <?php } ?>
                                  </div>
                              <?php } ?>
                          <?php } ?>
                      </div>
                  </div>
              <?php } ?>
          </div>

      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>