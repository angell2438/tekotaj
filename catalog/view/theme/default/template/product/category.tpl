<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">

    <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-md-9 col-sm-12'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
        <div class="hidden">
            <h1 class="hidden"><?php echo $heading_title; ?></h1>
            <?php if ($thumb || $description) { ?>
                <div class="row">

                </div>
                <hr>
            <?php } ?>
            <?php if ($categories) { ?>
                <h3><?php echo $text_refine; ?></h3>
                <?php if (count($categories) <= 5) { ?>
                    <div class="row">
                        <div class="col-sm-3">
                            <ul>
                                <?php foreach ($categories as $category) { ?>
                                    <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="row">
                        <?php foreach (array_chunk($categories, ceil(count($categories) / 4)) as $categories) { ?>
                            <div class="col-sm-3">
                                <ul>
                                    <?php foreach ($categories as $category) { ?>
                                        <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                    <?php } ?>
                                </ul>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            <?php } ?>
        </div>

      <?php if ($products) { ?>
          <div class="sort-wrapper">
              <div class="row">
                  <div class="flex-row flex-colum-xs">
                      <div class="col-md-4 col-sm-3 col-xs-12">
                          <div class="sort-text">
                              <?php echo $results; ?>
                          </div>
                      </div>
                      <div class="col-md-8 col-sm-9 col-xs-12">
                          <div class="flex-row sort-text-left flex-colum-xxs">
                              <div class="flex-row flex-colum-xxs sort-select">
                                  <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                                  <select id="input-sort" class="form-control" onchange="location = this.value;">
                                      <?php foreach ($sorts as $sorts) { ?>
                                          <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                              <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                          <?php } else { ?>
                                              <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                          <?php } ?>
                                      <?php } ?>
                                  </select>
                              </div>
                              <div class="flex-row flex-colum-xxs limit-select">
                                  <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                                  <select id="input-limit" class="form-control" onchange="location = this.value;">
                                      <?php foreach ($limits as $limits) { ?>
                                          <?php if ($limits['value'] == $limit) { ?>
                                              <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                          <?php } else { ?>
                                              <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                          <?php } ?>
                                      <?php } ?>
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      <div class="row">
          <?php foreach ($products as $product) { ?>
              <?php $this->partial('product_item', array(
                  'product' => $product,
                  'button_cart' => $button_cart,
                  'button_wishlist' => $button_wishlist,
                  'text_timer' => $text_timer,
                  'button_compare' => $button_compare,
                  'by_one_click' => $by_one_click )); ?>
          <?php } ?>
      </div>
      <div class="row">
        <div class="col-sm-12 text-left"><?php echo $pagination; ?></div>
      </div>
      <?php } ?>
      <?php if (!$categories && !$products) { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      </div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $content_bottom; ?>
<?php echo $footer; ?>
