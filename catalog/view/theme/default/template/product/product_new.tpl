<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container" >
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>">
            <?php echo $content_top; ?>
            <div class="row">
                <?php if ($column_left || $column_right) { ?>
                    <?php $class = 'col-md-6 col-sm-12'; ?>
                <?php } else { ?>
                    <?php $class = 'col-md-6 col-sm-12'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?>">
                    <?php if ($thumb || $images) { ?>
                        <?php if(count($images) >= 1) { ?>
                            <div class="slider-image-big-curier clearfix">
                                <div class="col-md-2">
                                    <div class="thumbnails-mini js-height" id="thumbnail_imgs">
                                        <div class="item ">
                                            <img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                        </div>
                                        <?php $k = '';
                                        foreach ($images as $image) {
                                            $k++; ?>
                                            <?php if ($image): ?>
                                                <div class="item ">
                                                    <img itemprop="image" src="<?php echo $image['thumb']; ?>"
                                                         data-link="image-<?php echo $k; ?>"
                                                         class="img-responsive"
                                                         title="<?php echo $heading_title; ?>"
                                                         alt="<?php echo $heading_title; ?>"/>
                                                </div>
                                            <?php endif;  ?>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="col-md-10">
                                    <div class="thumbnails js-height" id="preview_imgs">
                                        <div class="item thumbnails-pop">
                                            <?php if ($sales) { ?>
                                                <div class="plash top-sales">
                                                    <span><?php echo $sales; ?>%</span>
                                                </div>
                                            <?php } ?>

                                            <?php if ($news) { ?>
                                                <div class="plash new-prod">
                                                    <span>NEW</span>
                                                </div>
                                            <?php } ?>
                                            <?php if ($top) { ?>
                                                <div class="plash top-prod">
                                                    <span>top</span>
                                                </div>
                                            <?php } ?>
                                            <a  href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                                                <img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                            </a>
                                        </div>
                                        <?php $k = '';
                                        foreach ($images as $image) {
                                            $k++; ?>
                                            <div class="item thumbnails-pop">
                                                <?php if ($news) { ?>
                                                    <div class="new-product">
                                                        <p>NEW</p>
                                                    </div>
                                                <?php } ?>
                                                <a href="<?php echo $image['popup']; ?>">
                                                    <img itemprop="image" src="<?php echo $image['thumb']; ?>"
                                                         data-link="image-<?php echo $k; ?>"
                                                         class="img-responsive"
                                                         title="<?php echo $heading_title; ?>"
                                                         alt="<?php echo $heading_title; ?>"/>
                                                </a>
                                            </div>

                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <ul id="lightgallery" style="display: none">
                                <li data-src="<?php echo $popup; ?>" id="image-0">
                                    <a href="">
                                        <img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>">
                                    </a>
                                </li>
                                <?php $k = '';
                                foreach ($images as $image) {
                                    $k++; ?>

                                    <li data-src="<?php echo $image['popup']; ?>" id="image-<?php echo $k; ?>">
                                        <a href="<?php echo $image['popup']; ?>">
                                            <img src="<?php echo $image['thumb']; ?>" alt="<?php echo $heading_title; ?>">
                                        </a>
                                    </li>

                                <?php } ?>
                            </ul>
                        <?php } else { ?>
                            <?php if ($thumb) { ?>
                                <div class="slider-image-big js-height">
                                    <div class="item thumbnails-pop">
                                        <a  class="pop-img" href="<?php echo $popup; ?>" title="<?php echo $heading_title; ?>">
                                            <img itemprop="image" src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" />
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>

                </div>
                <?php if ($column_left || $column_right) { ?>
                    <?php $class = 'col-md-6 col-sm-12'; ?>
                <?php } else { ?>
                    <?php $class = 'col-md-6 col-sm-12'; ?>
                <?php } ?>
                <div class="<?php echo $class; ?>">
                    <h1><?php echo $heading_title; ?></h1>
                    <div class="flex-row flex-content">
                        <p class="status-prod"><span class="icon-checked"></span><?php echo $stock; ?></p>
                        <?php if ($review_status) { ?>
                            <div class="rating">
                                <?php for ($i = 1; $i <= 5; $i++) { ?>
                                    <?php if ($rating < $i) { ?>
                                        <span class="fa fa-stack "><i class="icon-star"></i></span>
                                    <?php } else { ?>
                                        <span class="fa fa-stack active"><i class="icon-star"></i></span>
                                    <?php } ?>
                                <?php } ?>
                                <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><?php echo $reviews; ?></a>
                            </div>

                        <?php } ?>
                    </div>

                    <div id="product">
                        <?php if ($model) { ?>
                            <div class="attribute_groups">
                                <div>
                                    <span class="attribute-name"> <?php echo $text_model; ?> </span>
                                    <span><?php echo $model; ?></span>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if (isset($attribute_body_groups)) { ?>
                            <div class="attribute_groups">
                                <?php foreach ($attribute_body_groups as $attribute_group) { ?>
                                    <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                        <div>
                                            <span class="attribute-name"><?php echo $attribute['name']; ?>:</span>
                                            <span><?php echo $attribute['text']; ?></span>
                                        </div>
                                    <?php } ?>
                                <?php } ?>
                            </div>
                        <?php } ?>

                        <?php if ($options) { ?>
                            <div class="row">
                                <?php foreach ($options as $option) { ?>
                                    <?php if ($option['type'] == 'select') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <select name="option[<?php echo $option['product_option_id']; ?>]" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control">
                                                    <option value=""><?php echo $option['name']; ?></option>
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'radio') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'checkbox') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox" name="option[<?php echo $option['product_option_id']; ?>][]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'image') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option_value['product_option_value_id']; ?>" />
                                                                <img src="<?php echo $option_value['image']; ?>" alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>" class="img-thumbnail" /> <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'text') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'textarea') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <textarea name="option[<?php echo $option['product_option_id']; ?>]" rows="5" placeholder="<?php echo $option['name']; ?>" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control"><?php echo $option['value']; ?></textarea>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'file') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <button type="button" id="button-upload<?php echo $option['product_option_id']; ?>" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-default btn-block"><i class="fa fa-upload"></i> <?php echo $button_upload; ?></button>
                                                <input type="hidden" name="option[<?php echo $option['product_option_id']; ?>]" value="" id="input-option<?php echo $option['product_option_id']; ?>" />
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'date') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group date">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'datetime') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group datetime">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                    <?php if ($option['type'] == 'time') { ?>
                                        <div class="col-md-6">
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label" for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group time">
                                                    <input type="text" name="option[<?php echo $option['product_option_id']; ?>]" value="<?php echo $option['value']; ?>" data-date-format="HH:mm" id="input-option<?php echo $option['product_option_id']; ?>" class="form-control" />
                                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            </div>
                                        </div>
                                    <?php } ?>
                                <?php } ?>

                            </div>
                        <?php } ?>
                        <div class="flex-row flex-content">
                            <?php if ($price) { ?>

                                <div class="discount">
                                    <?php if (!$special) { ?>
                                        <div>
                                            <?php echo $text_tags; ?> <span><?php echo $price; ?></span>

                                        </div>
                                    <?php } else { ?>
                                        <div>
                                            <?php echo $text_tags; ?>  <span><?php echo $special; ?></span>
                                        </div>
                                    <?php } ?>
                                </div>

                            <?php } ?>
                            <?php if ($discounts) { ?>
                                <?php  foreach ($discounts as $discount) { ?>
                                    <div class="discount">
                                        <?php echo $text_discount; ?> <?php echo $discount['quantity']; ?> шт):
                                        <span> <?php echo $discount['price']; ?></span>
                                    </div>
                                <?php } ?>
                            <?php } ?>

                        </div>
                        <?php if ($minimum > 1) { ?>
                            <div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $text_minimum; ?></div>
                        <?php } ?>
                        <div class="flex-row flex-row__m">
                            <div class="qty_block">
                                <div class="wrapp_inputs">
                                    <div class="button button_minus"><i class="icon-right-chevron-1"></i></div>
                                    <input type="text" name="quantity" value="<?php echo $minimum; ?>" data-min="<?php echo $minimum; ?>" size="2" id="input-quantity"/>
                                    <div class="button button_plus"><i class="icon-right-chevron-1"></i></div>
                                </div>
                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                            </div>

                            <button class='btn btn-pink_by' onclick="get_popup_purchase('<?php echo $product_id; ?>');"><span><?php echo $button_click_by; ?></span></button>
                            <button type="button" id="button-cart" data-loading-text="<?php echo $text_loading; ?>" class="btn btn-pink_by  violet">
                                <i class="icon-shopping-cart-1"></i>
                                <?php echo $button_cart; ?>
                            </button>

                        </div>
                        <div class="flex-row flex-content sm-flex-position">
                            <div class="btn-group">
                                <button type="button" data-toggle="tooltip" class="in_wishlist wishlist-js <?php echo $wishlist_status? 'disabled': '' ?>" <?php echo $wishlist_status? 'disabled': '' ?> onclick="wishlist.add('<?php echo $product_id; ?>');">
                                    <i class="icon-like-black-heart-button"></i><?php echo $button_wishlist; ?>
                                </button>
                            </div>

                            <div class="share_wrapp flex-row">

                                <div class="icon_wrapp flex-row">

                                    <div class="share_item facebook  icon-facebook-logo">
                                        <a href="" onClick="openWin2();"> Facebook</a>
                                    </div>
                                    <div class="share_item twitter icon-twitter-logo-silhouette">
                                        <a class=""
                                           href="https://twitter.com/share"
                                           data-size="large"
                                           data-text="custom share text"
                                           data-url="https://dev.twitter.com/web/tweet-button"
                                           data-hashtags="example,demo"
                                           data-via="twitterdev"
                                           data-related="twitterapi,twitter"
                                           target="_blank"> Twitter
                                        </a>
                                    </div>
                                </div>

                                <div class="hidden">
                                    <div class="g-plus" data-action="share"></div>
                                    <a href="https://twitter.com/share" class="twitter-share-button" data-lang="ru"></a>
                                </div>

                                <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];
                                        if(!d.getElementById(id))
                                        {js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";
                                            fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");
                                </script>
                                <script>
                                    $('.facebook a').click(function (e) {
                                        e.preventDefault();
                                    })
                                    function openWin2() {
                                        myWin=open("http://www.facebook.com/sharer.php?u=<?php echo $og_url; ?>","displayWindow","width=520,height=300,left=350,top=170,status=no,toolbar=no,menubar=no");
                                    }
                                </script>
                            </div>
                        </div>

                    </div>




                </div>
                <div class="col-md-12 col-xs-12">
                    <div class="big-information">
                        <div class="wrapper-notification">
                            <p>
                                <?php echo $text_notification; ?>
                            </p>

                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab-description" data-toggle="tab"><?php echo $tab_description; ?></a></li>
                            <?php if ($attribute_groups) { ?>
                                <li><a href="#tab-specification" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                            <?php } ?>
                            <?php if ($review_status) { ?>
                                <li><a href="#tab-review" data-toggle="tab"><?php echo $tab_review; ?></a></li>
                            <?php } ?>

                            <li><a href="#tab-delivery" data-toggle="tab"><?php echo $text_delivery_title; ?></a></li>

                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab-description"><?php echo $description; ?></div>
                            <?php if ($attribute_groups) { ?>
                                <div class="tab-pane" id="tab-specification">
                                    <table class="table table-violet">
                                        <?php foreach ($attribute_groups as $attribute_group ) {  ?>
                                            <?php if($attribute_group['attribute_group_id'] != 3 && $attribute_group['attribute_group_id'] != 6) { ?>
                                                <tbody>
                                                <tr>
                                                    <td colspan="2"><strong><?php echo $attribute_group['name']; ?></strong></td>
                                                    <td>
                                                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                            <p class="grey-text">
                                                                <?php echo $attribute['name']; ?>:
                                                            </p>
                                                        <?php } ?>
                                                    </td>
                                                    <td>
                                                        <?php foreach ($attribute_group['attribute'] as $attribute) { ?>
                                                            <p>
                                                                <?php echo $attribute['text']; ?>
                                                            </p>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            <?php } ?>
                                        <?php } ?>
                                    </table>
                                </div>
                            <?php } ?>
                            <?php if ($review_status) { ?>
                                <div class="tab-pane" id="tab-review">

                                    <div id="review"></div>
                                    <div class="c-form-style clearfix">

                                        <form class="form-horizontal white-style-form" id="form-review">
                                            <h2><?php echo $text_write; ?></h2>
                                            <?php if ($review_guest) { ?>
                                                <div class="clearfix">
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <input type="text" name="name" value="" id="input-name" class="form-control" placeholder="<?php echo $entry_name; ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group required">
                                                            <input type="text" name="email" value="" id="input-email" class="form-control" placeholder="<?php echo $entry_email; ?>"/>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="required">
                                                            <div class="c-reviewStars-input">
                                                                <p class="c-rating">Рейтинг</p>
                                                                <div class="reviewStars-input">
                                                                    <input type="radio" name="rating" id="star-5" value="5">
                                                                    <label for="star-5" class="star"> <i class="icon-star"></i></label>
                                                                    <input type="radio" name="rating" id="star-4" value="4">
                                                                    <label for="star-4" class="star"> <i class="icon-star"></i></label>
                                                                    <input type="radio" name="rating" id="star-3" value="3">
                                                                    <label for="star-3" class="star"> <i class="icon-star"></i></label>
                                                                    <input type="radio" name="rating" id="star-2" value="2">
                                                                    <label for="star-2" class="star"> <i class="icon-star"></i></label>
                                                                    <input type="radio" name="rating" id="star-1" value="1">
                                                                    <label for="star-1" class="star"> <i class="icon-star"></i></label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>


                                                <div class="form-group required">
                                                    <div class="col-sm-12">
                                                        <textarea name="text" rows="5" id="input-review" class="form-control" placeholder="<?php echo $entry_review; ?>"></textarea>
                                                    </div>
                                                </div>
                                                <div class="buttons c-center clearfix">
                                                    <button type="button" id="button-review"
                                                            data-loading-text="<?php echo $text_loading; ?>"
                                                            class="btn btn-default"><?php echo $entry_good; ?></button>

                                                </div>
                                            <?php } else { ?>
                                                <?php echo $text_login; ?>
                                            <?php } ?>
                                        </form>
                                    </div>

                                </div>
                            <?php } ?>
                            <div class="tab-pane" id="tab-delivery">

                                <?php echo $delivery_information; ?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <?php if ($products) { ?>
                <div class="releted-prod">
                    <h3><?php echo $text_related; ?></h3>
                    <div class="wrapper_prod-slider clearfix">
                        <?php foreach ($products as $product) { ?>
                            <?php $this->partial('product_item_module', array(
                                'product' => $product,
                                'button_cart' => $button_cart,
                                'text_tax' => $text_tax,
                                'button_wishlist' => $button_wishlist,
                                'button_compare' => $button_compare,
                                'text_opt'=> $text_opt,
                                'by_one_click' => $by_one_click,
                                'text_rozn'=> $text_rozn)); ?>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
            <?php if ($tags) { ?>
                <p><?php echo $text_tags; ?>
                    <?php for ($i = 0; $i < count($tags); $i++) { ?>
                        <?php if ($i < (count($tags) - 1)) { ?>
                            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
                        <?php } else { ?>
                            <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
                        <?php } ?>
                    <?php } ?>
                </p>
            <?php } ?>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function(){
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#recurring-description').html('');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#button-cart').on('click', function() {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function() {
                $('#button-cart').button('loading');
            },
            complete: function() {
                $('#button-cart').button('reset');
            },
            success: function(json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    var alert_elem = $('<div class="alert_add_cart">' + json['success'] + '<span class="rem_alert"></span></div>');
                    $(alert_elem).appendTo('.special_block_info .block_info');

                    setTimeout(function () {
                        $(alert_elem).fadeOut(400, function () {
                            $(alert_elem).remove();
                        });
                    },2500);
                    // $('#content').parent().before('<div class="alert alert-success"> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart > a').html('<i class="icon-shopping-cart-1"></i><span id="cart-total" class="count_item"> ' + json['total'] + '</span>');
                    }, 100);
                    $(".btn-primary").blur();
                    // $('.breadcrumb').after('<div class="alert alert-success">' + json['success'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                    $('#cart > a').html('<i class="icon-shopping-cart-1"></i><span id="cart-total" class="count_item"> ' + json['total'] + '</span>');

                    $('html, body').animate({ scrollTop: 0 }, 'slow');

                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function() {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }

        timer = setInterval(function() {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function() {
                        $(node).button('loading');
                    },
                    complete: function() {
                        $(node).button('reset');
                    },
                    success: function(json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('#review').delegate('.pagination a', 'click', function(e) {
        e.preventDefault();

        $('#review').fadeOut('slow');

        $('#review').load(this.href);

        $('#review').fadeIn('slow');
    });

    $('#review').load('index.php?route=product/product/review&product_id=<?php echo $product_id; ?>');

    $('#button-review').on('click', function() {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function() {
                $('#button-review').button('loading');
            },
            complete: function() {
                $('#button-review').button('reset');
            },
            success: function(json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });

    $(document).ready(function() {
        $('.thumbnails').magnificPopup({
            type:'image',
            delegate: 'a',
            gallery: {
                enabled:true
            }
        });
    });
    //--></script>
<?php echo $footer; ?>
