
<?php if ($reviews) { ?>
    <div class="rewiew_wrapp">
        <?php foreach ($reviews as $review) { ?>
            <div class="review_item">
                <div class="head flex-row">
                    <div class="left flex-row">
                        <div class="name"><?php echo $review['author']; ?></div>
                        <div class="flex-row testimonial-time">
                            <i class="icon icon-clock-circular-outline"></i>
                            <span class="time"><?php echo $review['date_added']; ?></span>
                        </div>
                    </div>
                    <div class="rating">
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                            <?php if ($review['rating'] < $i) { ?>
                                <span class="fa fa-stack "><i class="fa fa-star"></i></span>
                            <?php } else { ?>
                                <span class="fa fa-stack active"><i class="fa fa-star"></i></span>
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>

                <div class="text"><p><?php echo $review['text']; ?></p></div>

            </div>
        <?php } ?>
    </div>
    <div class="row">
        <div class="col-sm-12"><?php echo $pagination; ?></div>
    </div>
<?php } else { ?>
    <p><?php echo $text_no_reviews; ?></p>
<?php } ?>
