<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">
  <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
      <h1 class="hidden"><?php echo $heading_title; ?></h1>
        <?php if ($products) { ?>
            <div class="row">
                <div class="col-md-3 col-sm-4 hidden-xs">
                    <div class="table-left row-compare">
                        <div class="item-compare">
                            <p><?php echo $text_product; ?></p>
                        </div>
                        <div class="item-compare">
                            <p><?php echo $text_rating; ?></p>
                        </div>
                        <?php foreach ($attribute_groups as $attribute_group) { ?>
                            <div class="item-compare">
                                <p>
                                    <?php echo $attribute_group['name']; ?>
                                </p>
                            </div>
                        <?php } ?>
                        <div class="item-compare">
                            <p><?php echo $text_price; ?></p>
                        </div>
                        <div class="item-compare">
                            <p><?php echo $text_dimension; ?></p>
                        </div>

                    </div>
                </div>
                <div class="col-md-9 col-sm-8 col-xs-12">
                    <div class="row">
                        <div class="slider-wrapper table_right">
                            <?php foreach ($products as $product) { ?>
                                <div class="item-product row-compare col-lg-4 col-md-4 col-sm-6 col-xs-6 col-xxs-12">
                                    <div class="item-compare product-layout ">
                                        <div class=" product-thumb transition">
                                            <a class="mini-btn-close" href="<?php echo $product['remove']; ?>" ><i class="icon-close"></i></a>
                                            <div class="image">
                                                <a href="<?php echo $product['href']; ?>">
                                                    <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-responsive" />
                                                </a>
                                            </div>
                                            <div class="caption">
                                                <h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
                                                <?php if ($product['price']) { ?>
                                                    <p class="price">
                                                        <?php if (!$product['special']) { ?>
                                                            <?php echo $product['price']; ?>
                                                        <?php } else { ?>
                                                            <span class="price-new"><?php echo $product['special']; ?></span>
                                                            <span class="price-old"><?php echo $product['price']; ?></span>
                                                        <?php } ?>
                                                    </p>
                                                <?php } ?>
                                            </div>
                                            <div class="button-group">
                                                <?php if ($product['compare_status']){ ?>
                                                    <button class="mini-btn compare-js active" type="button" >
                                                        <i class="icon-bars-graphic"></i>
                                                    </button>
                                                <?php } else { ?>
                                                    <button class="mini-btn compare-js" type="button"  onclick="compare.add('<?php echo $product['product_id']; ?>');">
                                                        <i class="icon-bars-graphic"></i>
                                                    </button>
                                                <?php } ?>

                                                <?php if ($product['wishlist_status']){ ?>
                                                    <button class="mini-btn wishlist-js active" type="button" ><i class="icon-like-black-heart-button"></i></button>

                                                <?php } else { ?>
                                                    <button class="mini-btn wishlist-js" type="button"  onclick="wishlist.add('<?php echo $product['product_id']; ?>');">
                                                        <i class="icon-like-black-heart-button"></i></button>
                                                <?php } ?>
                                                <button class="mini-btn" type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');"><i class="icon-shopping-purse-icon-1"></i></button>
                                                <button class="one_click" onclick="get_popup_purchase(<?php echo $product['product_id']; ?>);" ><?php echo $by_one_click; ?></button>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="item-compare bold-text">
                                        <div class="rating">
                                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                <?php if ($product['rating'] < $i) { ?>
                                                    <span class="fa fa-stack "><i class="fa fa-star"></i></span>
                                                <?php } else { ?>
                                                    <span class="fa fa-stack active"><i class="fa fa-star"></i></span>
                                                <?php } ?>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php foreach ($attribute_groups as $attribute_group) { ?>
                                        <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
                                            <div class="item-compare bold-text">
                                                <?php if (isset($product['attribute'][$key])) { ?>
                                                    <p><?php echo $product['attribute'][$key]; ?></p>
                                                <?php } else { ?>
                                                    <p>-</p>
                                                <?php } ?>
                                            </div>
                                        <?php } ?>
                                    <?php } ?>
                                    <div class="item-compare bold-text">
                                        <p><?php if ($product['price']) { ?>
                                                <?php if (!$product['special']) { ?>
                                                    <?php echo $product['price']; ?>
                                                <?php } else { ?>
                                                        <!-- <strike><?php echo $product['price']; ?></strike>  --><?php echo $product['special']; ?>
                                                <?php } ?>
                                            <?php } ?></p>
                                    </div>
                                    <div class="item-compare bold-text">
                                        <p><?php echo $product['length']; ?> x <?php echo $product['width']; ?> x <?php echo $product['height']; ?></p>
                                    </div>



                                </div>

                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php } else { ?>
            <p><?php echo $text_empty; ?></p>
            <div class="buttons">
                <div class=""><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
        <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
