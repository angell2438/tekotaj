<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
    <div class="container">

        <div class="row"><?php echo $column_left; ?>
            <?php if ($column_left && $column_right) { ?>
                <?php $class = 'col-sm-6'; ?>
            <?php } elseif ($column_left || $column_right) { ?>
                <?php $class = 'col-sm-9'; ?>
            <?php } else { ?>
                <?php $class = 'col-sm-12'; ?>
            <?php } ?>
            <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
                <h1><?php echo $heading_title; ?></h1>

                <div class="row attribute_groups">
                    <div class="col-sm-4">
                        <input type="text" name="search" value="<?php echo $search; ?>" placeholder="<?php echo $text_keyword; ?>" id="input-search" class="form-control grey-input" />
                    </div>
                    <div class="col-sm-3">
                        <select name="category_id" class="form-control">
                            <option value="0"><?php echo $text_category; ?></option>
                            <?php foreach ($categories as $category_1) { ?>
                                <?php if ($category_1['category_id'] == $category_id) { ?>
                                    <option value="<?php echo $category_1['category_id']; ?>" selected="selected"><?php echo $category_1['name']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $category_1['category_id']; ?>"><?php echo $category_1['name']; ?></option>
                                <?php } ?>
                                <?php foreach ($category_1['children'] as $category_2) { ?>
                                    <?php if ($category_2['category_id'] == $category_id) { ?>
                                        <option value="<?php echo $category_2['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $category_2['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_2['name']; ?></option>
                                    <?php } ?>
                                    <?php foreach ($category_2['children'] as $category_3) { ?>
                                        <?php if ($category_3['category_id'] == $category_id) { ?>
                                            <option value="<?php echo $category_3['category_id']; ?>" selected="selected">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                                        <?php } else { ?>
                                            <option value="<?php echo $category_3['category_id']; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $category_3['name']; ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="col-sm-3">

                    </div>
                </div>
                <div class="row attribute_groups">
                    <label class="form-checkbox checkbox-inline">
                        <?php if ($sub_category) { ?>
                            <input type="checkbox" name="sub_category" value="1" checked="checked" />
                        <?php } else { ?>
                            <input type="checkbox" name="sub_category" value="1" />
                        <?php } ?>
                        <span class="form-checkbox__marker"></span>
                        <span class="form-checkbox__label"><?php echo $text_sub_category; ?></span>
                    </label>
                    <label class="form-checkbox checkbox-inline">
                        <?php if ($description) { ?>

                            <input type="checkbox" name="description" value="1" id="description" checked="checked" />
                        <?php } else { ?>

                            <input type="checkbox" name="description" value="1" id="description" />
                        <?php } ?>
                        <span class="form-checkbox__marker"></span>
                        <span class="form-checkbox__label"><?php echo $entry_description; ?></span>
                    </label>
                </div>
                <div class="attribute_groups">
                    <input type="button" value="<?php echo $button_search; ?>" id="button-search" class="btn btn-primary" />

                </div>
                <div class="attribute_groups">
                <h2><?php echo $text_search; ?></h2>
                </div>
                <?php if ($products) { ?>
                    <div class="sort-wrapper">
                        <div class="row">
                            <div class="flex-row flex-colum-xs">
                                <div class="col-md-4 col-sm-3 col-xs-12">
                                    <div class="sort-text">
                                        <?php echo $results; ?>
                                    </div>
                                </div>
                                <div class="col-md-8 col-sm-9 col-xs-12">
                                    <div class="flex-row sort-text-left flex-colum-xxs">
                                        <div class="flex-row flex-colum-xxs sort-select">
                                            <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                                            <select id="input-sort" class="form-control" onchange="location = this.value;">
                                                <?php foreach ($sorts as $sorts) { ?>
                                                    <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                                        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <div class="flex-row flex-colum-xxs limit-select">
                                            <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                                            <select id="input-limit" class="form-control" onchange="location = this.value;">
                                                <?php foreach ($limits as $limits) { ?>
                                                    <?php if ($limits['value'] == $limit) { ?>
                                                        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                                    <?php } else { ?>
                                                        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <?php foreach ($products as $product) { ?>
                            <?php $this->partial('product_item_module', array(
                                'product' => $product,
                                'button_cart' => $button_cart,
                                'button_wishlist' => $button_wishlist,
                                'text_timer' => $text_timer,
                                'button_compare' => $button_compare,
                                'by_one_click' => $by_one_click )); ?>
                        <?php } ?>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-left"><?php echo $pagination; ?></div>
                    </div>
                <?php } else { ?>
                    <p><?php echo $text_empty; ?></p>
                <?php } ?>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
    <script type="text/javascript"><!--
        $('#button-search').bind('click', function() {
            url = 'index.php?route=product/search';

            var search = $('#content input[name=\'search\']').prop('value');

            if (search) {
                url += '&search=' + encodeURIComponent(search);
            }

            var category_id = $('#content select[name=\'category_id\']').prop('value');

            if (category_id > 0) {
                url += '&category_id=' + encodeURIComponent(category_id);
            }

            var sub_category = $('#content input[name=\'sub_category\']:checked').prop('value');

            if (sub_category) {
                url += '&sub_category=true';
            }

            var filter_description = $('#content input[name=\'description\']:checked').prop('value');

            if (filter_description) {
                url += '&description=true';
            }

            location = url;
        });

        $('#content input[name=\'search\']').bind('keydown', function(e) {
            if (e.keyCode == 13) {
                $('#button-search').trigger('click');
            }
        });

        $('select[name=\'category_id\']').on('change', function() {
            if (this.value == '0') {
                $('input[name=\'sub_category\']').prop('disabled', true);
            } else {
                $('input[name=\'sub_category\']').prop('disabled', false);
            }
        });

        $('select[name=\'category_id\']').trigger('change');
        --></script>
<?php echo $footer; ?>