<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">

  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

      <?php if ($description) { ?>
      <div class="row hidden">
        <?php if ($thumb) { ?>
        <div class="col-sm-2"><img src="<?php echo $thumb; ?>" alt="<?php echo $heading_title; ?>" title="<?php echo $heading_title; ?>" class="img-thumbnail" /></div>
        <?php } ?>
        <?php if ($description) { ?>
        <div class="col-sm-10"><?php echo $description; ?></div>
        <?php } ?>
      </div>
      <?php } ?>
        <?php if ($products) { ?>
<!--            <div class="row">-->
                <div class="sort-wrapper">
                    <div class="row">
                        <div class="flex-row flex-colum-xs">
                            <div class="col-md-4 col-xs-12">
                                <div class="sort-text name-brends">
                                    <h1><?php echo $heading_title; ?></h1>
                                </div>
                            </div>
                            <div class="col-md-8 col-xs-12">
                                <div class="flex-row sort-text-left">
                                    <div class="flex-row sort-select">
                                        <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                                        <select id="input-sort" class="form-control" onchange="location = this.value;">
                                            <?php foreach ($sorts as $sorts) { ?>
                                                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                                    <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="flex-row limit-select">
                                        <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                                        <select id="input-limit" class="form-control" onchange="location = this.value;">
                                            <?php foreach ($limits as $limits) { ?>
                                                <?php if ($limits['value'] == $limit) { ?>
                                                    <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                                <?php } else { ?>
                                                    <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        <div class="row">
            <?php foreach ($products as $product) { ?>
                <?php $this->partial('product_item_module', array(
                    'product' => $product,
                    'button_cart' => $button_cart,
                    'button_wishlist' => $button_wishlist,
                    'button_compare' => $button_compare,
                    'text_timer' => $text_timer,
                    'by_one_click' => $by_one_click )); ?>
            <?php } ?>
                </div>
                <div class="row">
                    <div class="col-sm-12"><?php echo $pagination; ?></div>
                </div>
<!--            </div>-->

      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>