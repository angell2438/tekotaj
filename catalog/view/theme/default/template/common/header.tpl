<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>

    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/reject/reject.css" media="all" />
    <script src="/reject/reject.min.js"></script>
    <![endif]-->
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $og_url; ?>" />
    <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>" />
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>" />
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>" />
    <?php if($gtm) { ?>
        <!-- Google Tag Manager -->
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
                    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
                j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
                'https://www.googletagmanager.com/gtm.js?
                id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','<?php echo $gtm; ?>');</script>
        <!-- End Google Tag Manager -->
    <?php } ?>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/bootstrap/js/bootstrap.min.js"></script>
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="catalog/view/javascript/jquery/slick/slick-theme.css" rel="stylesheet">
    <link href="catalog/view/javascript/jquery/slick/slick.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/select2.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<!--    <link href="catalog/view/javascript/jquery/fancybox/jquery.fancybox.css" rel="stylesheet">-->
    <link href="catalog/view/javascript/jquery/flipclock/flipclock.css" rel="stylesheet">
    <!--    <link href="catalog/view/theme/default/stylesheet/fonts.css" rel="stylesheet">-->
    <link href="catalog/view/theme/default/stylesheet/main.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/sergo.css" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main_information.css" rel="stylesheet">

    <link href="catalog/view/theme/default/stylesheet/media.css" rel="stylesheet">
    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>

    <script src="catalog/view/javascript/oauth/oauth.js"></script>
    <script src="catalog/view/javascript/common.js"></script>
    <script src="//npmcdn.com/isotope-layout@3/dist/isotope.pkgd.js"></script>
    <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
        <?php echo $analytic; ?>
    <?php } ?>
</head>
<body class="<?php echo $class; ?>">
<?php if($gtm) { ?>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo $gtm; ?>>"
                      height="0" width="0"
                      style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php } ?>
<nav id="top" class="top">
    <div class="container">
        <div class="row">
            <div class="visible-sm visible-xs">
                <div class="col-sm-6 col-xs-offset-2">
                    <div class="logo-menu ">
                        <?php if ($logo) { ?>
                            <?php if ($home == $og_url) { ?>
                                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                            <?php } else { ?>
                                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                            <?php } ?>
                        <?php } else { ?>
                            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-md-7 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="wrapper-top-menu">
                        <ul class="list-inline">
                            <li><a href="<?php echo $informations[4]['href']; ?>"><?php echo $informations[4]['title']; ?></a></li>
                            <li><a href="<?php echo $informations[6]['href']; ?>"><?php echo $informations[6]['title']; ?></a></li>
                            <li class="dropdown">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                                    <?php echo $text_client; ?>
                                    <i class="icon-down-arrow-of-angle"></i>
                                </a>
                                <div class="dropdown-menu">
                                    <div class="dropdown-inner">
                                        <ul>
                                            <li><a href="<?php echo $informations[7]['href']; ?>"><?php echo $informations[7]['title']; ?></a></li>
                                            <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
                                            <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
                                            <li><a href="<?php echo $price; ?>"><?php echo $informations[9]['title']; ?></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            <?php if ($ncategories) { ?>
                                <?php foreach ($ncategories as $ncat) { ?>
                                    <li><a href="<?php echo $ncat['href']; ?>"><?php echo $ncat['name']; ?></a></li>
                                <?php } ?>
                            <?php } ?>
                            <li><a href="<?php echo $gallery; ?>"><?php echo $text_all; ?></a></li>
                            <li><a href="<?php echo $testimonial; ?>"><?php echo $text_testimonial; ?></a></li>
                            <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                        </ul>
                    </div>
                </div>

            </div>
            <div class="col-md-5 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="flex-row content-flex">
                        <div class="contact-info">
                            <a class="phone-shop" href="mailto:<?php echo  $mail; ?>">
                                <?php echo $mail; ?>
                            </a>
                            <a class="phone-shop" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>">
                                <?php echo $telephone; ?>
                            </a>

                        </div>
                        <div class="currency-language flex-row">
                            <?php echo $language; ?>
                            <?php echo $currency; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</nav>
<header>
    <div class="container">
        <div class="row  flex-row flex-row__sm not-flex">
            <div class="visible-sm visible-xs col-sm-1 col-xs-2">
                <button class="open-xs-menu js-information-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
            <div class="col-md-5 col-sm-6 col-xs-10">
                <div class="row">
                    <div class="logo">
                        <?php if ($logo) { ?>
                            <?php if ($home == $og_url) { ?>
                                <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                            <?php } else { ?>
                                <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                            <?php } ?>
                        <?php } else { ?>
                            <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                        <?php } ?>
                    </div>
                </div>

            </div>
            <div class="col-md-7 col-sm-5 col-xs-12">
                <div class="row">
                    <div class="flex-row mini-position">
                        <div class="col-lg-8 col-md-7">
                            <div class="row">
                                <div class="visible-sm visible-xs">
                                    <div class="count-wrapper search-sm">
                                        <a class="" href="<?php echo $search_link; ?>"><i class="fa fa-search"></i></a>
                                    </div>
                                </div>
                                <div class="hidden-sm hidden-xs">
                                    <?php echo $search; ?>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-5 js-position-menu">
                            <div class="row">
                                <div class="flex-row">
                                    <div class="count-wrapper compare_wrapp<?php echo ($text_compare > 0)? ' has_product':''?>">
                                        <a href="<?php echo $compare ; ?>">
                                            <i class="icon-bars-graphic" aria-hidden="true"></i>
                                            <span class="count_item">
                                                <?php echo $text_compare; ?>
                                            </span>
                                        </a>
                                    </div>
                                    <div class="count-wrapper wishlist_wrapp<?php echo ($text_wishlist > 0)? ' has_product':''?>">
                                        <a href="<?php echo $wishlist; ?>">
                                            <i class="icon-like-black-heart-button" aria-hidden="true"></i>
                                            <span class="count_item">
                                                <?php echo $text_wishlist; ?>
                                            </span>
                                        </a>
                                    </div>
                                    <?php echo $cart; ?>
                                    <div class="count-wrapper login-block dropdown">

                                        <?php if ($logged): ?>
                                            <a href="<?= $account ?>" title="<?php echo $text_account; ?>">
                                                <i class="icon-user"></i>
                                            </a>
                                        <?php else: ?>
                                            <a href="" title="<?php echo $text_account; ?>"  data-toggle="modal" data-target="#modal-authorization">
                                                <i class="icon-user"></i>
                                            </a>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>




                </div>
            </div>
        </div>
        <div class="special_block_info">
            <div class="block_info">
            </div>
        </div>
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="row-item">
                <hr>
                <div class="wrapper-menu">
                    <div class="visible-xs">
                        <button class="open-xs-menu js-catalog-menu">
                            <img src="catalog/view/image/menu-1.svg" alt="menu">
                            <span class="name-caalog">
                                <?php echo $text_category; ?>
                            </span>
                            <i class="icon-down-arrow-of-angle"></i>
                        </button>
                    </div>
                    <?php if ($categories) { ?>
                        <nav id="menu" class="navbar">
                            <div class="navbar-collapse navbar-ex1-collapse">
                                <ul class="nav navbar-nav">
                                    <?php $k=''; foreach ($categories as $category) { $k++; ?>
                                        <?php if ($category['children']) { ?>
                                            <li class="dropdown-list collapsed">
                                                <a class="list-notactive"
                                                   data-toggle="collapse"
                                                   href="#multiCollapseExample<?php echo $k; ?>"
                                                   role="button"
                                                   aria-expanded="false"
                                                   aria-controls="multiCollapseExample<?php echo $k; ?>">
                                                    <?php echo $category['name']; ?>
                                                </a>
                                            </li>
                                        <?php } else { ?>
                                            <li><a href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a></li>
                                        <?php } ?>
                                    <?php } ?>
                                </ul>
                                <?php $k=''; foreach ($categories as $category) { $k++; ?>
                                    <?php if ($category['children']) { ?>
                                        <div class="wrapper-menu-collaps">
                                            <div id="multiCollapseExample<?php echo $k; ?>" class="dropdown-level multi-collapse collapse clearfix">
                                                <?php foreach (array_chunk($category['children'], ceil(count($category['children']) / $category['column'])) as $children) { ?>
                                                    <ul class="list-unstyled clearfix">
                                                        <?php foreach ($children as $child) { ?>
                                                            <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
                                                        <?php } ?>
                                                    </ul>
                                                <?php } ?>
                                            </div>
                                        </div>

                                    <?php } ?>
                                <?php } ?>
                            </div>
                        </nav>
                    <?php } ?>
                </div>
            </div>

        </div>
    </div>
</header>
<div class="main-content">


