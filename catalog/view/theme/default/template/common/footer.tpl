</div>
<footer>
    <div class="container">
        <div class="row">
            <?php if ($informations) { ?>
                <div class="col-sm-3 col-sm-12 parent-elements">
                    <h3 class="footer-title"><?php echo $text_information; ?><i class="visible-xs fa fa-angle-down" aria-hidden="true"></i></h3>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo $informations[4]['href']; ?>"><?php echo $informations[4]['title']; ?></a></li>
                        <li><a href="<?php echo $informations[6]['href']; ?>"><?php echo $informations[6]['title']; ?></a></li>
                        <?php if ($ncategories) { ?>
                            <?php foreach ($ncategories as $ncat) { ?>
                                <li><a href="<?php echo $ncat['href']; ?>"><?php echo $ncat['name']; ?></a></li>
                            <?php } ?>
                        <?php } ?>
                        <li><a href="<?php echo $testimonial; ?>"><?php echo $text_testimonial; ?></a></li>
                        <li><a href="<?php echo $contact; ?>"><?php echo $text_contact; ?></a></li>
                    </ul>
                </div>
            <?php } ?>
            <div class="col-sm-3 col-sm-12 parent-elements">
                <h3 class="footer-title"><?php echo $text_client; ?><i class="visible-xs fa fa-angle-down" aria-hidden="true"></i></h3>
                <ul class="list-unstyled">
                    <li><a href="<?php echo $informations[7]['href']; ?>"><?php echo $informations[7]['title']; ?></a></li>
                    <li><a href="<?php echo $manufacturer; ?>"><?php echo $text_manufacturer; ?></a></li>
                    <li><a href="<?php echo $voucher; ?>"><?php echo $text_voucher; ?></a></li>
                    <li><a href="<?php echo $price; ?>"><?php echo $informations[9]['title']; ?></a></li>
                </ul>

            </div>
            <div class="col-sm-3 col-sm-12 parent-elements">
                <h3 class="footer-title"><?php echo $text_service; ?><i class="visible-xs fa fa-angle-down" aria-hidden="true"></i></h3>
                <ul class="list-unstyled">
                    <?php foreach ($categories as $category) { ?>
                        <li>
                            <a href="<?php echo $category['href']; ?>" data-hover="<?php echo $category['name']; ?>">
                                <?php echo $category['name']; ?>
                            </a>
                        </li>
                    <?php } ?>
                </ul>

            </div>
            <div class="col-sm-3 col-sm-12 parent-elements">
                <h3 class="footer-title"><?php echo $text_contact; ?><i class="visible-xs fa fa-angle-down" aria-hidden="true"></i></h3>
                <ul class="list-unstyled">
                    <li>
                        <p><?php echo $address; ?></p>
                    </li>
                    <li>
                        <a class="phone-shop" href="mailto:<?php echo $email; ?>">
                            <?php echo $email; ?>
                        </a>
                    </li>
                    <li>
                        <a class="phone-shop" href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>">
                            <?php echo $telephone; ?>
                        </a>
                    </li>

                </ul>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="row">
                    <div class="flex-row flex-column">
                        <div class="col-md-9 col-sm-8 col-xs-12">
                            <div class="row">
                                <?php echo $newsletter; ?>
                            </div>
                        </div>

                        <div class="col-md-3 col-sm-4 col-xs-12">
                            <div class="row">
                                <ul class="social clearfix">
                                    <li><a href="<?php echo $config_fb; ?>" target="_blank"><i class="icon-facebook-logo"></i></a></li>
                                    <li><a href="<?php echo $config_inst; ?>" target="_blank"><i class="icon-instagram-1"></i></a></li>
                                    <li><a href="<?php echo $config_twitter; ?>" target="_blank"><i class="icon-twitter-logo-silhouette"></i></a></li>
                                    <li><a href="<?php echo $config_you; ?>" target="_blank"><i class="icon-youtube"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</footer>
<div class="copiright">
    <div class="container">
        <div class="row flex-row flex-column">
            <div class="col-md-6 col-xs-12">
                <p><?php echo $powered; ?></p>

            </div>
            <div class="col-md-6 col-xs-12">
                <a href="http://web-systems.solutions/" target="_blank">
                    <img src="/catalog/view/image/group.svg" alt="web-systems.solutions" title="web-systems.solutions" width="151" height="19">
                </a>
            </div>
        </div>
    </div>
</div>
<?php echo $quicksignup; ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<!--<script src="catalog/view/javascript/jquery/fancybox/jquery.fancybox.pack.js"></script>-->
<script src="catalog/view/javascript/jquery/flipclock/flipclock.min.js"></script>
<script src="catalog/view/javascript/jquery/slick/slick.js"></script>
<script src="catalog/view/javascript/jquery/jquery.maskedinput.min.js"></script>
<script src="catalog/view/javascript/jquery/select2.min.js"></script>
<script src="catalog/view/javascript/jquery/machheigh.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
<script src="https://tympanus.net/TipsTricks/DirectionAwareHoverEffect/js/modernizr.custom.97074.js"></script>
<script src="catalog/view/javascript/jquery/jquery.elevateZoom-3.0.8.min.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lightgallery.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-fullscreen.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-thumbnail.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-video.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-autoplay.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-zoom.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-hash.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-pager.js"></script>
<!--<script src="catalog/view/javascript/masonry.pkgd.min.js" type="text/javascript"></script>-->
<script src="catalog/view/javascript/dotdotdot.min.js"></script>
<script src="catalog/view/javascript/main.js"></script>
</body></html>