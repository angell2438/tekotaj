<div id="cart" class="cart-wrapper count-wrapper">
    <a href="<?php echo $checkout; ?>" class="btn btn-cart">
        <i class="icon-shopping-purse-icon-1"></i>
        <span id="cart-total" class="count_item"><?php echo $text_items; ?></span>
    </a>
</div>
