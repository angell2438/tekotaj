<div class="modal fade authorization_modal" id="modal-authorization" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span class="icon-close" aria-hidden="true"></span>
            </button>

            <div class="modal-body">
                <h3 class="modal-title"><?= $text_signin_register ?></h3>
                <div class="row">
                    <div class="col-md-6 col-sm-12 authorization_modal-login" id="quick-login">

                        <div class="form-group required">
                            <input type="text" name="email" value="" class="form-control" placeholder="<?php echo $entry_email; ?>">
                        </div>
                        <div class="form-group required">
                            <input type="password" name="password" value="" class="form-control" placeholder="<?php echo $entry_password; ?>">
                        </div>
                        <div class="fl_login">
                            <div class="show_pass_ch">
                                <label class="form-checkbox price-position" for="show_pass">
                                    <input type="checkbox" id="show_pass"/>
                                    <span class="form-checkbox__marker"></span>
                                    <span class="form-checkbox__label"><?php echo $text_show_pass; ?></span>
                                </label>
                            </div>
                            <a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a>
                        </div>


                        <div class="buttons">
                            <button type="button" class="btn btn-primary loginaccount"  data-loading-text="<?php echo $text_loading; ?>"><span><?php echo $text_login ?></span></button>
                        </div>
                        <div class="wrapper-out ">
                            <p><?php echo $text_social_login; ?></p>
                            <div class="flex-row flex-column-xs">
                                <div class="authorization_modal-login_icons_item " onclick="dix_facebook()"><span class="icon-style icon-facebook-outlined-logo"></span> <span>Facebook</span></div>
                                <div class="authorization_modal-login_icons_item " onclick="dix_googlePop('/glogin', 'ds', 700, 500)"><span class="icon-style icon-google-plus"></span> <span>Google+</span></div>
                            </div>

                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12 authorization_modal-register" id="quick-register">

                        <div class="form-group required">
                            <input id="input-email2" type="text" name="email" value="" class="form-control" placeholder="<?php echo $entry_email; ?>">
                        </div>
                        <div class="form-group required">
                            <input id="input-password2" type="password" name="password" value="" class="form-control" placeholder="<?php echo $entry_password; ?>">
                        </div>
                        <div class="form-group required">
                            <input id="input-name1" type="text" name="name" value="" class="form-control" placeholder="<?php echo $entry_name; ?>">
                        </div>
                        <div class="form-group required">
                            <input id="input-lastname1" type="text" name="lastname" value="" class="form-control" placeholder="<?php echo $entry_lastname; ?>">
                        </div>

                        <div class="fl_login">
                            <div class="show_pass_ch">
                                <label class="form-checkbox price-position" for="show_pass_r">
                                    <input type="checkbox" id="show_pass_r"/>
                                    <span class="form-checkbox__marker"></span>
                                    <span class="form-checkbox__label"><?php echo $text_show_pass; ?></span>
                                </label>
                            </div>
                        </div>
                        <div class="buttons">
                            <?php if ($text_agree) { ?>
                                <input type="checkbox" name="agree" value="1" />&nbsp;<?php echo $text_agree; ?>
                                <br><br>
                                <button type="button" class="btn btn-primary createaccount"  data-loading-text="<?php echo $text_loading; ?>" ><span><?php echo $text_register; ?></span></button>
                            <?php }else{ ?>
                                <button type="button" class="btn  btn-default orang createaccount" data-loading-text="<?php echo $text_loading; ?>" ><span><?php echo $text_register; ?></span></button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $(document).delegate('.quick_authorization', 'click', function(e) {
        e.preventDefault();
        $('#modal-authorization').modal('show');
    });
</script>
<script>
    $('#quick-register input').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('#quick-register .createaccount').trigger('click');
        }
    });
    $('#quick-register .createaccount').click(function() {
        $.ajax({
            url: 'index.php?route=common/quicksignup/register',
            type: 'post',
            data: $('#quick-register input[type=\'text\'], #quick-register input[type=\'password\'], #quick-register input[type=\'checkbox\']:checked'),
            dataType: 'json',
            beforeSend: function() {
                $('#quick-register .createaccount').button('loading');
                $('#quick-register .alert-danger').remove();
            },
            complete: function() {
                $('#quick-register .createaccount').button('reset');
            },
            success: function(json) {
                $('#modal-register .form-group').removeClass('has-error');
                $('.text-danger').remove();
                if(json['islogged']){
                    window.location.href="index.php?route=account/account";
                }
                if (json['error_email']) {
                    $('#quick-register #input-email2').parent().addClass('has-error');
                    $('#quick-register #input-email2').focus();
                }

                if (json['error_password']) {
                    $('#quick-register #input-password2').parent().addClass('has-error');
                    $('#quick-register #input-password2').focus();
                }

                if (json['error_name']) {
                    $('#quick-register #input-name1').parent().addClass('has-error');
                    $('#quick-register #input-name1').focus();
                }

                if (json['error_lastname']) {
                    $('#quick-register #input-lastname1').parent().addClass('has-error');
                    $('#quick-register #input-lastname1').focus();
                }

                if (json['error']) {
                    $('#modal-register .modal-header').after('<div class="alert alert-danger" style="margin:5px;"> ' + json['error'] + '</div>');
                }

                if (json['now_login']) {
                    $('.quick-login').before('<li class="dropdown"><a href="<?php echo $account; ?>" title="<?php echo $text_account; ?>" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <span class="hidden-xs hidden-sm hidden-md"><?php echo $text_account; ?></span> <span class="caret"></span></a><ul class="dropdown-menu dropdown-menu-right"><li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li><li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li><li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li><li><a href="<?php echo $download; ?>"><?php echo $text_download; ?></a></li><li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li></ul></li>');

                    $('.quick-login').remove();
                }
                if (json['success']) {
                    $('#modal-authorization .modal-title').html(json['heading_title']);
                    success = json['text_message'];
                    $('#modal-authorization .modal-body').addClass('message').html(success);
                    $('#modal-authorization').on('hidden.bs.modal', function () {
                        window.location.href="index.php?route=account/account";
                    });
                }
            }
        });
    });
</script>
<script>
    $('#quick-login input').on('keydown', function(e) {
        if (e.keyCode == 13) {
            $('#quick-login .loginaccount').trigger('click');
        }
    });
    $('#quick-login .loginaccount').click(function() {
        $.ajax({
            url: 'index.php?route=common/quicksignup/login',
            type: 'post',
            data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
            dataType: 'json',
            beforeSend: function() {
                $('#quick-login .loginaccount').button('loading');
                $('#quick-login .alert-danger').remove();
            },
            complete: function() {
                $('#quick-login .loginaccount').button('reset');
            },
            success: function(json) {
                $('#modal-login .form-group').removeClass('has-error');
                if(json['islogged']){
                    window.location.href="index.php?route=account/account";
                }

                if (json['error']) {
                    $('#quick-login').prepend('<div class="alert alert-danger">' + json['error'] + '</div>');

                    $('#quick-login input').parent().addClass('has-error');

                    $('#quick-login #input-email1').focus();
                }
                if(json['success']){
                    loacation();
                    $('#modal-login').modal('hide');
                }

            }
        });
    });
</script>
<script >
    function loacation() {
        location.reload();
    }
</script>