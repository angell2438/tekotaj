<?php echo $header; ?>
<div class="wrapper-errorpage">
    <div class="container">
        <div class="row">
            <div class="text-errors">
                <h1><?php echo $heading_title; ?></h1>
                <p><?php echo $text_error; ?></p>
                <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_home; ?></a>
            </div>
        </div>
    </div>
</div>

<?php echo $footer; ?>