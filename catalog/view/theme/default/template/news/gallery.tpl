<div class="article-content">
    <div class="article-meta">
        <?php if ($article) { ?>
            <div class="grid_wrapper products-container__margin">
                <div class="grid-sizer"></div>
                <?php foreach ($article as $articles) { ?>
                        <?php $k = 0; foreach ($articles['gallery_images'] as $gallery) { $k++;  ?>
                            <div class="grid-item <?php echo $k; ?> <?php if ($k=='4' || $k=='11') { echo "galery-grid-1";} else {echo "galery-grid-2"; } ?>">
                                <a class="colorbox project__image" href="<?php echo $gallery['popup']; ?>" title="<?php echo $gallery['text']; ?>">
                                    <img src="<?php echo $gallery['popup']; ?>" />
                                </a>
                            </div>
                        <?php } ?>

                <?php } ?>
            </div>
        <?php } ?>
    </div>
</div>
<script><!--
    $(document).ready(function () {
        var window_h = $(window).height();
        var window_w = $(window).width();

        if ((window_w >= 768)) {

            console.log(window_w);
            $('.grid_wrapper').isotope({
                itemSelector: '.grid-item',
                percentPosition: true,
                masonry: {
                    columnWidth: '.grid-sizer',
                }
            });
            $("a.colorbox").fancybox({
                'transitionIn'	:	'elastic',
                'transitionOut'	:	'elastic',
                'speedIn'		:	600,
                'speedOut'		:	200,
                'overlayShow'	:	false
            });
        } else {
            $('.grid_wrapper').isotope('destroy');
        }
    });
//--></script>