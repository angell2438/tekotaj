<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs)); ?>
    <div class="container">
        <div class="row">
            <div id="content" class="col-sm-12"><?php echo $content_top; ?>
                <h1 class="hidden"><?php echo $heading_title; ?></h1>
                <?php if ($images) { ?>
                    <div class="gallery_list">
                        <?php foreach (array_chunk($images, 5) as $image_rows) { ?>
                            <div class="gallery_row row <?= (count($image_rows) == 1) ? 'row_one' : ''; ?>">
                                <?php $k = 1; foreach ($image_rows as $image_row) { ?>
                                    <?php if($k == 1 || $k == 2){?>
                                        <?php if ($k == 1) { ?>
                                            <div class="flex-row">
                                        <?php } ?>
                                        <div class="gallery_item">
                                            <div class="wrapp_thumb">
                                                <a href="<?= $image_row['thumb']; ?>" data-fancybox="gallery">
                                                    <img src="<?= $image_row['image']; ?>" alt="<?= $image_row['title']; ?>"
                                                         class="img-responsive">
                                                </a>
                                            </div>
                                        </div>
                                        <?php if ($k == 2) { ?>
                                            </div>
                                        <?php } ?>
                                    <?php }else{ ?>
                                            <?php if ($k == 3) { ?>
                                                <div class="small_image flex-row">
                                            <?php } ?>
                                            <div class="gallery_item">
                                                <div class="wrapp_thumb">
                                                    <a href="<?= $image_row['thumb']; ?>" data-fancybox="gallery">
                                                        <img src="<?= $image_row['image']; ?>" alt="<?= $image_row['title']; ?>"
                                                             class="img-responsive">
                                                    </a>
                                                </div>
                                            </div>
                                            <?php if ($k == count($image_rows)) { ?>
                                                </div>
                                            <?php } ?>
                                    <?php } ?>
                                    <?php $k++;  } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } else { ?>
                    <p><?= $text_empty; ?></p>
                    <div class="text-right">
                        <a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a>
                    </div>
                <?php } ?>
                <?php echo $pagination; ?>
                <?php echo $content_bottom; ?></div>
            <?php echo $column_right; ?></div>
    </div>
<?php echo $footer; ?>