<?php echo $header; ?>
<?php $this->partial('breadcrumbs', array('breadcrumbs' => $breadcrumbs));?>
<div class="container">
    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
            <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
            <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
            <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
            <h1 class="hidden"><?php echo $heading_title; ?></h1>
            <div class="block-information-contact">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="contact-info_big js-height">
                            <i class="icon-placeholder"></i>
                            <p><?php echo $text_address; ?></p>
                            <address>
                                <?php echo $address; ?>
                            </address>
                        </div>

                    </div>
                    <div class="col-sm-4">
                        <div class="contact-info_big js-height">
                            <i class="icon-phone-call2"></i>
                            <p><?php echo $text_telephone; ?></p>
                            <div class="flex-row">
                                <?php if ($telephone) { ?>

                                    <a href="tel:<?php echo preg_replace("/[ ()-]/", "", $telephone) ;?>">
                                        <?php echo $telephone; ?>
                                    </a>
                                <?php } ?>
                                <?php if ($fax) { ?>
                                    <a  href="tel:<?php echo preg_replace("/[ ()-]/", "", $fax) ;?>">
                                        / <?php echo $fax; ?>
                                    </a>
                                <?php } ?>
                            </div>


                        </div>

                    </div>
                    <div class="col-sm-4">
                        <div class="contact-info_big js-height">
                            <i class="icon-envelope"></i>
                            <p><?php echo $text_comment; ?></p>
                            <?php if ($mail) { ?>
                                <a href="mailto:<?php echo  $mail; ?>">
                                    <?php echo $mail; ?>
                                </a>
                            <?php } ?>
                        </div>

                    </div>
                </div>
            </div>
            <div class="c-form-style">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal white-style-form">

                    <h2><?php echo $text_contact; ?></h2>
                    <div class="clearfix">
                        <div class="col-md-6">
                            <div class="form-group required">
                                <input type="text" name="name" value="<?php echo $name; ?>" id="input-name_contact" class="form-control" placeholder="<?php echo $entry_name; ?>"/>
                                <?php if ($error_name) { ?>
                                    <div class="text-danger"><?php echo $error_name; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group required">
                                <input type="text" name="email" value="<?php echo $email; ?>" id="input-email_contact" class="form-control"  placeholder="<?php echo $entry_email; ?>"/>
                                <?php if ($error_email) { ?>
                                    <div class="text-danger"><?php echo $error_email; ?></div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>


                    <div class="form-group required">
                        <div class="col-sm-12">
                            <textarea name="enquiry" rows="10" id="input-enquiry_contact" class="form-control" placeholder="<?php echo $entry_enquiry; ?>"></textarea>
                            <?php if ($error_enquiry) { ?>
                                <div class="text-danger"><?php echo $error_enquiry; ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="buttons c-center clearfix">
                        <input class="btn btn-default orang" type="submit" value="<?php echo $button_send; ?>" />

                    </div>
                </form>
            </div>
            <?php echo $content_bottom; ?></div>
        <?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
