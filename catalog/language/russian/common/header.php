<?php
// Text
$_['text_home']          = 'Главная';
$_['text_wishlist']      = '%s';
$_['text_compare']      = '<span class="count">%s</span>';
$_['text_shopping_cart'] = 'Корзина покупок';
$_['text_category']      = 'Каталог продукции';
$_['text_account']       = 'Личный кабинет';
$_['text_register']      = 'Регистрация';
$_['text_login']         = 'Авторизация';
$_['text_order']         = 'История заказов';
$_['text_transaction']   = 'История платежей';
$_['text_download']      = 'Файлы для скачивания';
$_['text_logout']        = 'Выход';
$_['text_checkout']      = 'Оформление заказа';
$_['text_search']        = 'Поиск';
$_['text_all']           = 'Галерея';
$_['text_page']          = 'страница';
$_['text_testimonial']          = 'Отзывы';
