<?php
// Text
$_['text_information']  = 'Информация';
$_['text_service']      = 'продукция';
$_['text_extra']        = 'Дополнительно';
$_['text_contact']      = 'Контакты';
$_['text_return']       = 'Возврат товара';
$_['text_sitemap']      = 'Карта сайта';
$_['text_manufacturer'] = 'Производители';
$_['text_voucher']      = 'Подарочные сертификаты';
$_['text_affiliate']    = 'Партнёры';
$_['text_special']      = 'Товары со скидкой';
$_['text_account']      = 'Мы в соцсетях';
$_['text_order']        = 'История заказов';
$_['text_wishlist']     = 'Мои Закладки';
$_['text_newsletter']   = 'Рассылка новостей';
$_['text_powered']      = 'Tecotaj.com.ua © Все права защищены';

$_['entry_name']      	= 'Ваше имя';
$_['entry_phone']       = 'Номер мобильного телефона';
$_['text_call']      	= 'Заказать звонок';
$_['text_send']      	= 'Отправить';
$_['text_loading']      = 'Обработка';