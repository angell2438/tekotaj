<?php
// Heading
$_['heading_title']  = 'Контакты';

// Text
$_['text_location']  = 'Наш адрес';
$_['text_store']     = 'Наши магазины';
$_['text_contact']   = 'Контактная форма';
$_['text_address']   = 'Адрес ';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'По вопросам опта:';
$_['text_comment']   = 'Электронная почта ';
$_['text_success']   = '<p>Ваш запрос был успешно отправлен администрации магазина!</p>';

// Entry
$_['entry_name']     = 'Ваше имя';
$_['entry_phone']    = 'Телефон';
$_['entry_email']    = 'Email';
$_['entry_enquiry']  = 'Сообщение';

// Email
$_['email_subject']  = 'Сообщение %s';

// Errors
$_['error_name']     = 'Имя должно быть от 3 до 32 символов!';
$_['error_email']    = 'E-Mail указан некорректно!';
$_['error_phone']    = 'Телефон указан некорректно!';
$_['error_enquiry']  = 'Сообщение должно быть от 10 до 3000 символов!';
