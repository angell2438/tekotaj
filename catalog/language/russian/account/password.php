<?php
// Heading 
$_['heading_title']  = 'Смена пароля';

// Text
$_['text_account']   = 'Личный Кабинет';
$_['text_success']   = 'Ваш пароль успешно изменен!';

// Entry
$_['entry_old_password']   = 'Текущий пароль';
$_['entry_password'] = 'Пароль';
$_['entry_confirm']  = 'Подтвердить новый прароль';

// Error
$_['error_old_password'] = 'Неверно введен старый пароль';
$_['error_password'] = 'Пароль должен быть от 4 до 20 символов!';
$_['error_confirm']  = 'Пароли не совпадают!';
$_['text_password'] = 'Смена пароля';