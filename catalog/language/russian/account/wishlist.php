<?php
// Heading 
$_['heading_title'] = 'Выбраные';

// Text
$_['text_account']  = 'Личный Кабинет';
$_['text_instock']  = 'В наличии';
$_['text_wishlist'] = 'Закладки (%s)';
$_['text_login']    = 'Необходимо войти в <a href="%s">Личный Кабинет</a> или <a href="%s">создать учетную запись</a>, чтобы добавить товар <a href="%s">%s</a> в свои <a href="%s">закладки</a>!';
$_['text_success']  = 'Товар успешно добавлен в закладки!';
$_['text_remove']   = 'Список закладок успешно обновлен!';
$_['text_empty']    = 'Ваши закладки пусты';

// Column
$_['column_image']  = 'Изображение';
$_['column_name']   = 'Наименование товара';
$_['column_model']  = 'Модель';
$_['column_stock']  = 'На складе';
$_['column_price']  = 'Цена';
$_['column_action'] = 'Действие';