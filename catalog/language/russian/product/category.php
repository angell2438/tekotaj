<?php
// Text
$_['text_refine']       = 'Уточнить поиск';
$_['text_product']      = 'Товары';
$_['text_error']        = 'Категория не найдена!';
$_['text_empty']        = 'В этой категории нет товаров.';
$_['text_quantity']     = 'Кол-во:';
$_['text_manufacturer'] = 'Производитель:';
$_['text_model']        = 'Код товара:';
$_['text_points']       = 'Бонусные Баллы:';
$_['text_price']        = 'Цена:';
$_['text_tax']          = 'Без налога:';
$_['text_compare']      = 'Сравнение товаров (%s)';
$_['text_sort']         = 'Сортировка:';
$_['text_default']      = 'новинки';
$_['text_name_asc']     = 'По Имени (A - Я)';
$_['text_name_desc']    = 'По Имени (Я - A)';
$_['text_price_asc']    = 'от дешевых к дорогим';
$_['text_price_desc']   = 'от дорогих к дешевым';
$_['text_rating_asc']   = 'популярні';
$_['text_rating_desc']  = 'По Рейтингу (убыванию)';
$_['text_model_asc']    = 'По Модели (A - Я)';
$_['text_model_desc']   = 'По Модели (Я - A)';
$_['text_limit']        = 'Показывать по:';
