<?php
// Text
$_['text_search']                             = 'Поиск';
$_['text_brand']                              = 'Производитель';
$_['text_manufacturer']                       = 'Производитель:';
$_['text_model']                              = 'Код товара:';
$_['text_reward']                             = 'Бонусные баллы:';
$_['text_points']                             = 'Цена в Бонусных баллах:';
$_['text_stock']                              = 'Доступность:';
$_['text_instock']                            = 'На складе';
$_['text_tax']                                = 'Без НДС:';
$_['text_discount']                           = 'Оптова цена (от ';
$_['text_option']                             = 'Доступные опции';
$_['text_minimum']                            = 'Минимальное кол-во для заказа этого товара: %s. ';
$_['text_reviews']                            = 'Отзывов: <span>%s</span> ';
$_['text_write']                              = 'Написать отзыв';
$_['text_login']                              = 'Пожалуйста <a href="%s"> авторизуйтесь</a> или <a href="%s"> зарегистрируйтесь</a> для просмотра';
$_['text_no_reviews']                         = 'Нет отзывов об этом товаре.';
$_['text_note']                               = '<span class="text-danger">Внимание:</span> HTML не поддерживается! Используйте обычный текст!';
$_['text_success']                            = 'Спасибо за Ваш отзыв. Он был направлен на модерацию.';
$_['text_related']                            = 'Похожие товары';
$_['text_tags']                               = 'Розничная цена:';
$_['text_error']                              = 'Товар не найден!';
$_['text_payment_recurring']                    = 'Платежные профили';
$_['text_trial_description']                  = '%s каждый %d %sй для %d оплат(ы),';
$_['text_payment_description']                = '%s каждый %d %s(-и) из %d платежей(-а)';
$_['text_payment_cancel']                     = '%s every %d %s(s) until canceled';
$_['text_day']                                = 'день';
$_['text_week']                               = 'неделя';
$_['text_semi_month']                         = 'полмесяца';
$_['text_month']                              = 'месяц';
$_['text_year']                               = 'год';
$_['text_viewed']                             = 'Товары, которые просматривали';
// Entry
$_['entry_qty']                               = 'Кол-во';
$_['entry_name']                              = 'Ваше имя';
$_['entry_review']                            = 'Сообщение';
$_['entry_email']                             = 'Электронная почта';
$_['entry_rating']                            = 'Рейтинг';
$_['entry_good']                              = 'Отправить';
$_['entry_bad']                               = 'Плохо';

// Tabs
$_['tab_description']                         = 'Описание';
$_['tab_attribute']                           = 'Размеры';
$_['tab_review']                              = 'Отзывы';

// Error
$_['error_name']                              = 'Имя должно быть от 2 до 25 символов!';
$_['error_email']                              = 'Email введен не верно!';
$_['error_text']                              = 'Текст Отзыва должен быть от 25 до 1000 символов!';
$_['error_rating']                            = 'Пожалуйста поставьте оценку!';