<?php
// Text
$_['text_signin_register']    = 'Авторизація';
$_['text_login']   			  = 'Увійти';
$_['text_register']    		  = 'Зареєструватися';

$_['text_new_customer']       = 'Реєстрація';
$_['text_returning']          = 'У мене вже є обліковий запис';
$_['text_social_login']       = 'Увійти через соціальні мережі';
$_['text_returning_customer'] = 'I am a returning customer';
$_['text_details']            = 'Your Personal Details';
$_['entry_email']             = 'Електронна пошта';
$_['entry_name']              = 'Ваше ім\'я';
$_['entry_password']          = 'Пароль';
$_['entry_lastname']          = 'Прізвище';
$_['entry_repassword']        = 'Повторіть пароль';
$_['entry_telephone']         = 'Номер телефону';
$_['text_forgotten']          = 'Забули пароль?';
$_['text_agree']              = 'I agree to the <a href="%s" class="agree"><b>%s</b></a>';
$_['text_show_pass']          = 'Показати пароль';


//Button
$_['button_login']            = 'Login';

//Error
$_['error_name']           = 'Ім\'я повинно бути від 2 до 32 символів!';
$_['error_lastname']       = 'Прізвище повинно бути від 2 до 32 символів!';
$_['error_email']          = 'E-Mail невірний!';
$_['error_telephone']      = 'Телефон повинен бути від 3 до 32 символів!';
$_['error_password']       = 'Пароль повинен бути від 4 до 20 символів!';
$_['error_repassword']     = 'Паролі не співпадають!';
$_['error_exists']         = 'E-Mail вже зареєстрований!';
$_['error_agree']          = 'Вы должны согласиться с %s!';
$_['error_warning']        = 'Будь ласка, уважно перевірте форму на наявність помилок!';
$_['error_approved']       = 'Ваша учетная запись требует одобрения, прежде чем Вы можете войти в систему.';
$_['error_login']          = 'E-Mail не зареєстрований.';