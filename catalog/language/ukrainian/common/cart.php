<?php
// Text
$_['text_items']     = '<span class="count">%s</span>';
$_['text_empty']     = 'У кошику порожньо!';
$_['text_cart']      = 'Відкрити кошик';
$_['text_checkout']  = 'Оформити замовлення';
$_['text_recurring'] = 'Профіль платежу';