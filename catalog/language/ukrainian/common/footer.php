<?php
// Text
$_['text_information']  = 'Інформація';
$_['text_service']      = 'продукція';
$_['text_extra']        = 'Додатково';
$_['text_contact']      = 'Контакти';
$_['text_return']       = 'Повернення товару';
$_['text_sitemap']      = 'Мапа сайту';
$_['text_manufacturer'] = 'Виробники';
$_['text_voucher']      = 'Подарункові сертифікати';
$_['text_affiliate']    = 'Партнери';
$_['text_special']      = 'Товари зі знижкою';
$_['text_account']      = 'Ми в соцмережах';
$_['text_order']        = 'Історія замовлень';
$_['text_wishlist']     = 'Мої закладки';
$_['text_newsletter']   = 'Розсилка новин';
$_['text_powered']      = 'Tecotaj.com.ua © Всі права захищені.';

$_['entry_name']      	= 'Ваше ім\'я';
$_['entry_phone']       = 'Номер мобільного телефону';
$_['text_call']      	= 'Замовити дзвінок';
$_['text_send']      	= 'Відправити';
$_['text_loading']      = 'Опрацювання';