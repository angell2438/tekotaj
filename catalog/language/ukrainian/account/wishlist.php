<?php
// Heading 
$_['heading_title'] = 'Обрані';

// Text
$_['text_account']  = 'Особистий кабінет';
$_['text_instock']  = 'В наявності';
$_['text_wishlist'] = 'Закладки (%s)';
$_['text_login']    = 'Необхідно увійти в <a href="%s"> Особистий Кабінет </a> або <a href="%s"> створити обліковий запис </a>, щоб додати товар <a href="%s">%s </a> в свої <a href="%s"> закладки </a>!';
$_['text_success']  = 'Товар успішно доданий до закладки!';
$_['text_remove']   = 'Перелік закладок успішно оновлено!';
$_['text_empty']    = 'Ваші закладки порожні';

// Column
$_['column_image']  = 'Зображення';
$_['column_name']   = 'Найменування товару';
$_['column_model']  = 'Модель';
$_['column_stock']  = 'На складі';
$_['column_price']  = 'Ціна';
$_['column_action'] = 'Дія';