<?php
// Heading 
$_['heading_title']      = 'Прайс листи';

// Text
$_['text_un_login1']     = 'Вибачте, але інформація даного розділу не доступна!';
$_['text_un_login2']     = 'Для того, щоб переглянути прайс листи, будь ласка, увійдіть під своїм обліковим записом.';
$_['text_authorization'] = 'Авторизація';
$_['download_price']      = 'Скачати прайс-лист';