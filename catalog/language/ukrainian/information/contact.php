<?php
// Heading
$_['heading_title']  = 'Контакти';

// Text
$_['text_location']  = 'Наша Адреса';
$_['text_store']     = 'Наші магазини';
$_['text_contact']   = 'Контактна форма';
$_['text_address']   = 'Адреса';
$_['text_telephone'] = 'Телефон';
$_['text_fax']       = 'Факс';
$_['text_open']      = 'З питань опту:';
$_['text_comment']   = 'Email';
$_['text_success']   = '<p>Ваш запит був успішно відправлений адміністрації магазину!</p>';

// Entry
$_['entry_name']     = 'Ваше ім’я';
$_['entry_phone']    = 'Телефон';
$_['entry_email']    = 'Email';
$_['entry_enquiry']  = 'Повідомлення';

// Email
$_['email_subject']  = 'Повідомлення %s';

// Errors
$_['error_name']     = 'Ім’я має бути від 3 до 32 символів!';
$_['error_email']    = 'E-Mail вказано некоректно!';
$_['error_phone']    = 'Телефон указан некорректно!';
$_['error_enquiry']  = 'Повідомлення має бути від 10 до 3000 символів!';
