<?php
// Text
$_['text_refine']       = 'Уточнити Пошук';
$_['text_product']      = 'Товари';
$_['text_error']        = 'Категорія не знайдена!';
$_['text_empty']        = 'У цій категорії немає товарів.';
$_['text_quantity']     = 'Кількість:';
$_['text_manufacturer'] = 'Виробник:';
$_['text_model']        = 'Модель:';
$_['text_points']       = 'Бонусні Бали:';
$_['text_price']        = 'Ціна:';
$_['text_tax']          = 'Без податку:';
$_['text_sort']         = 'Сортування:';
$_['text_default']      = 'новинки';
$_['text_name_asc']     = 'За Ім’ям (A - Я)';
$_['text_name_desc']    = 'За Ім’ям (Я - A)';
$_['text_price_asc']    = 'від дешевих до дорогих';
$_['text_price_desc']   = 'від дорогих до дешевих';
$_['text_rating_asc']   = 'популярные';
$_['text_rating_desc']  = 'За Рейтингом (зростання)';
$_['text_model_asc']    = 'За Моделлю (A - Я)';
$_['text_model_desc']   = 'За Моделлю (Я - A)';
$_['text_limit']        = 'Показувати по:';