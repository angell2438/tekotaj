<?php
// Heading
$_['heading_title']     = 'Порівняння товарів';

// Text
$_['text_product']      = 'Товар';
$_['text_name']         = 'Найменування';
$_['text_image']        = 'Зображення';
$_['text_price']        = 'Ціна';
$_['text_model']        = 'Модель';
$_['text_manufacturer'] = 'Виробник';
$_['text_availability'] = 'Наявність';
$_['text_instock']      = 'В наявності';
$_['text_rating']       = 'Рейтинг';
$_['text_reviews']      = 'На підставі %s відкликання(ів).';
$_['text_summary']      = 'Всього';
$_['text_weight']       = 'Вага';
$_['text_dimension']    = 'Розмір';
$_['text_compare']      = '<span class="count">%s</span>';
$_['text_success']      = 'Товар успішно доданий до Списку порівняння!';
$_['text_remove']       = 'Перелік товарів успішно оновлено!';
$_['text_empty']        = 'Ви не обрали жодного товару для порівняння.';