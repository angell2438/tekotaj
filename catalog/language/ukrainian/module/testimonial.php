<?php
// Text
$_['heading_title']	           = 'Відгуки';

$_['text_write']               = 'Написати відгук';
$_['text_login']               = 'Будь ласка, <a href="%s"> Увійдіть </a> або <a href="%s"> зареєструйтеся </a>, щоб переглянути';
$_['text_no_reviews']          = 'Відгуків немає.';
$_['text_note']                = '<span class="text-danger">Примітка: </span> HTML не використовувати!';
$_['text_success']             = 'Дякую за ваш відгук. Він був направлений на модерацію.';

$_['text_mail_subject']        = 'У вас є новий відгук (%s).';
$_['text_mail_waiting']	       = 'У вас є новий відгук.';
$_['text_mail_author']	       = 'Автор: %s';
$_['text_mail_rating']	       = 'Рейтинг: %s';
$_['text_mail_text']	       = 'Текст:';


$_['text_review_all']	       = 'Відгуки про магазин';
$_['text_review_prod']	       = 'Відгуки про товар';



// Entry
$_['entry_name']               = 'Ваше ім\'я';
$_['entry_review']             = 'Ваш відгук';
$_['entry_rating']             = 'Рейтинг';
$_['entry_good']               = 'Відправити';
$_['entry_bad']                = 'Плохо';

// Button
$_['button_continue']          = 'Відправити';

// Error
$_['error_name']                              = 'Ім’я має бути від 3 до 25 символів!';
$_['error_text']                              = 'Текст Відгуку має бути від 25 до 1000 символів!';
$_['error_rating']                            = 'Будь ласка, поставте оцінку!';
$_['error_captcha']            = 'Попередження: код підтвердження не відповідає зображенню';

