<?php
// Heading
$_['heading_title']     = 'Новини';

// Text
$_['text_headlines']    = 'всі новини';
$_['text_comments']     = 'комментарии к статье';
$_['text_comments_v']   = 'коментарии';
$_['button_more']       = 'Детальніше';
$_['text_posted_by']    = 'Posted by';
$_['text_posted_on']    = 'On';
$_['text_posted_pon']   = 'Опубликовано';
$_['text_posted_in']    = 'Added in';
$_['text_updated_on']   = 'Updated on';
$_['text_blogpage']     = 'Blog Headlines';
$_['text_more_news']    = 'Переглянути всі';
?>
