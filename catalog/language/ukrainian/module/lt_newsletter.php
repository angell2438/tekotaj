<?php
// Heading
$_['heading_title'] 		 = 'Підписка на розсилку';

// Text
$_['text_intro']	 		 = '';
$_['text_description']	 	 = 'Our promise to you: You will only receive the emails that you permitted upon subscription. Your email address will never be shared with any 3rd parties and you will receive only the type of content for which you signed up.';
$_['text_subscribed']   	 = 'Підписка оформлена!';
$_['text_unsubscribed']   	 = 'Ви вже підписані!';
$_['text_subject'] 			 = 'Підписка';
$_['text_message'] 			 = 'Email: %s';

//Fields
$_['entry_email'] 			 = 'Введіть email для підписки';

//Buttons
$_['text_button'] 			 = 'Підписатись';

//Error
$_['error_invalid'] 		 = 'Некоректний e-mail!';
