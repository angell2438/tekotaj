<?php
// $_['heading_title'] = 'Мега Фильтр';
// $_['name_price'] = 'Цена';
// $_['name_manufacturers'] = 'Производители';
// $_['name_rating'] = 'Рейтинг';
// $_['name_search'] = 'Поиск';
// $_['name_stock_status'] = 'Состояние';
// $_['name_location'] = 'Место нахождения';
// $_['name_length'] = 'Длина';
// $_['name_width'] = 'Ширина';
// $_['name_height'] = 'Высота';
// $_['name_mpn'] = 'MPN';
// $_['name_isbn'] = 'ISBN';
// $_['name_sku'] = 'SKU';
// $_['name_upc'] = 'UPC';
// $_['name_ean'] = 'EAN';
// $_['name_jan'] = 'JAN';
// $_['name_model'] = 'Модель';
// $_['text_button_apply'] = 'Подать заявление';
// $_['text_reset_all'] = 'Сбросить все';
// $_['text_show_more'] = 'Показать больше (%s)';
// $_['text_show_less'] = 'Показывать меньше';
// $_['text_display'] = 'Дисплей';
// $_['text_grid'] = 'Сетка';
// $_['text_list'] = 'Список';
// $_['text_loading'] = 'Загрузка...';
// $_['text_select'] = 'Выбрать...';
// $_['text_go_to_top'] = 'Go to top';
// $_['text_init_filter'] = 'Initialize the filter';
// $_['text_initializing'] = 'Initializing...';
?>
<?php
$_['heading_title'] = 'Фільтр';
$_['name_price'] = 'Ціна';
$_['name_manufacturers'] = 'Колекції';
$_['name_rating'] = 'Рейтинг';
$_['name_search'] = 'Пошук';
$_['name_stock_status'] = 'Статут на складе';
$_['name_location'] = 'Матеріал';
$_['name_length'] = 'Довжина';
$_['name_width'] = 'Ширина';
$_['name_height'] = 'Висота';
$_['name_mpn'] = 'Номер  колекції';
$_['name_isbn'] = 'Форма';
$_['name_sku'] = 'Артикул';
$_['name_upc'] = 'Колір';
$_['name_ean'] = 'Размір';
$_['name_jan'] = 'Ширина';
$_['name_model'] = 'Модель';


$_['text_button_apply'] = 'Применить';
$_['text_reset_all'] = 'Очистити фільтри';
$_['text_show_more'] = 'Показать еще (%s)';
$_['text_show_less'] = 'Скрыть';
$_['text_display'] = 'Показать';
$_['text_grid'] = 'Сетка';
$_['text_list'] = 'Лист';
$_['text_loading'] = 'Загрузка...';
$_['text_select'] = 'Селект...';
$_['text_go_to_top'] = 'Поднять на верх';
$_['text_init_filter'] = 'Инициализация фильтра';
$_['text_initializing'] = 'Инициализация...';
?>
