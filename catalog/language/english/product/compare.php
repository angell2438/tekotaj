<?php
// Heading
$_['heading_title']     = 'Product Comparison';

// Text
$_['text_product']      = 'Product';

$_['text_name']         = 'Product';
$_['text_image']        = 'Image';
$_['text_price']        = 'Price';
$_['text_model']        = 'Model';
$_['text_manufacturer'] = 'Brand';
$_['text_availability'] = 'Availability';
$_['text_instock']      = 'In Stock';
$_['text_rating']       = 'Rating';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_summary']      = 'Summary';
$_['text_weight']       = 'Weight';
$_['text_dimension']    = 'Dimensions';
$_['text_compare']      = '<span class="count">%s</span>';
$_['text_success']      = 'You have added to your product comparison!';
$_['text_remove']       = 'You have modified your product comparison!';
$_['text_empty']        = 'You have not chosen any products to compare.';