<?php
// Heading 
$_['heading_title']      = 'Price list';

// Text
$_['text_un_login1']     = 'Sorry, but the information in this section is not available!';
$_['text_un_login2']     = 'In order to view the price list, please log in with your account.';
$_['text_authorization'] = 'Authorization';
$_['download_price']      = 'Download price list';