<?php
// Text
$_['text_name']  			     = 'Name:';
$_['text_email']  				 = 'E-mail:';
$_['text_message'] 				 = 'Message:';
$_['text_phone']				 = 'Your phone';

$_['error_name']   				 = 'The name must contain between 2 and 25 characters!';
$_['error_email']   			 = 'Email is obligatory!';
$_['error_message'] 			 = 'Message has 10 characters!';
$_ ['error_phone'] = 'The phone must contain 10 characters!';
$_['success'] = 'Your request has been successfully sent!';

$_['email_subject_message'] = 'New message';
$_['email_subject_callback'] = 'New Call Order';
$_['text_call_info'] = 'Send us a phone number and we\'ll call you!';
