<?php
// Heading
$_['heading_title'] = '404';

// Text
$_['text_error']    = 'The page you requested cannot be found.';