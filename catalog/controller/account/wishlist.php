<?php
class ControllerAccountWishList extends Controller {
    public function index() {
//		if (!$this->customer->isLogged()) {
//			$this->session->data['redirect'] = $this->url->link('account/wishlist', '', 'SSL');
//
//			$this->response->redirect($this->url->link('account/login', '', 'SSL'));
//		}

        $this->load->language('account/wishlist');

        $this->load->model('account/wishlist');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        if (isset($this->request->get['remove'])) {

            foreach ($this->session->data['wishlist'] as $key => $val){
                if ($val == $this->request->get['remove']){
                    unset($this->session->data['wishlist'][$key]);
                }
            }

            // Remove Wishlist
            $this->model_account_wishlist->deleteWishlist($this->request->get['remove']);

            $this->session->data['success'] = $this->language->get('text_remove');

            $this->response->redirect($this->url->link('account/wishlist'));
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );


        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('account/wishlist')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_empty'] = $this->language->get('text_empty');

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_stock'] = $this->language->get('column_stock');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_action'] = $this->language->get('column_action');
        $data['text_timer'] = $this->language->get('text_timer');

        $data['text_sale'] = $this->language->get('text_sale');
        $data['text_new'] = $this->language->get('text_new');
        $data['button_purchase_now'] = $this->language->get('button_purchase_now');
        $data['text_rozn'] = $this->language->get('text_rozn');
        $data['text_opt'] = $this->language->get('text_opt');

        $data['button_continue'] = $this->language->get('button_continue');

        $data['text_tax'] = $this->language->get('text_tax');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['by_one_click'] = $this->language->get('by_one_click');
        $data['button_remove'] = $this->language->get('button_remove');

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }




        if ($this->customer->isLogged()) {
            $results = $this->model_account_wishlist->getWishlist();
        }else{
            if(!empty($this->session->data['wishlist'])){
                foreach ($this->session->data['wishlist'] as $res){
                    $results[] = array(
                        'product_id' => $res
                    );
                }
            }else{
                $results = array();
            }
        }


        $data['products'] = array();

        foreach ($results as $result) {
            $product_info = $this->model_catalog_product->getProduct($result['product_id']);

            if ($product_info) {

                $discounts = $this->model_catalog_product->getProductDiscounts($product_info['product_id']);

                $releted_discounts = array();
                foreach ($discounts as $discount) {
                    //  echo $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                    $releted_discounts[] = array(
                        'quantity' => $discount['quantity'],
                        'price'    => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax'))),
                        'price_value'   => $discount['price']
                    );
                }

                if ($product_info['image']) {
                    $image = $this->model_tool_image->resize($product_info['image'], $this->config->get('config_image_wishlist_width'), $this->config->get('config_image_wishlist_height'));
                } else {
                    $image = false;
                }

                if ($product_info['quantity'] <= 0) {
                    $stock = $product_info['stock_status'];
                } elseif ($this->config->get('config_stock_display')) {
                    $stock = $product_info['quantity'];
                } else {
                    $stock = $this->language->get('text_instock');
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                } else {
                    $tax = false;
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$product_info['special']) {
                    $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = $product_info['rating'];
                } else {
                    $rating = false;
                }

                if ($product_info['news']) {
                    $news = $product_info['news'];
                } else {
                    $news = false;
                }

                if ($product_info['sales'] AND isset($product_info['special'])) {
                    $sales = round(100-($product_info['special']*100/$product_info['price']),0);
                } else {
                    $sales = false;
                }


                if ($product_info['top']) {
                    $top = $product_info['top'];
                } else {
                    $top = false;
                }

                if (isset($this->session->data['wishlist']) && in_array($product_info['product_id'], $this->session->data['wishlist'])) {
                    $wishlist_status = '1';
                } else {
                    $wishlist_status = false;
                }

                if (isset($this->session->data['compare']) && in_array($product_info['product_id'], $this->session->data['compare'])) {
                    $compare_status = '1';
                } else {
                    $compare_status = false;
                }

                if (!empty($product_info['special_end_date']) AND ($product_info['special_end_date']!='0000-00-00')) {
                    $special_end_date = explode('-', $product_info['special_end_date']);
                    $count_end_date =$special_end_date[1].', '.$special_end_date[2].', '.$special_end_date[0];
                    $count_end_date = $product_info['special_end_date'];
                } else
                    $count_end_date = false;

                $data['products'][] = array(
                    'product_id'      => $product_info['product_id'],
                    'thumb'           => $image,
                    'model'           => $product_info['model'],
                    'sku'             => $product_info['sku'],
                    'name'            => $product_info['name'],
                    'description'     => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'           => $price,
                    'special'         => $special,
                    'tax'             => $tax,
                    'wishlist_status' => $wishlist_status,
                    'compare_status'  => $compare_status,
                    'news'            => $news,
                    'count_end_date'  => $count_end_date,
                    'discounts'       => $releted_discounts,
                    'sales'           => $sales,
                    'top'             => $top,
                    'rating'          => $rating,
                    'href'       => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                    'remove'     => $this->url->link('account/wishlist', 'remove=' . $product_info['product_id'])
                );
            } else {
                $this->model_account_wishlist->deleteWishlist($result['product_id']);
            }
        }

        $data['continue'] = $this->url->link('common/home', '', 'SSL');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/wishlist.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/wishlist.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/account/wishlist.tpl', $data));
        }
    }

    public function add() {
        $this->load->language('account/wishlist');

        $json = array();

        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
        } else {
            $product_id = 0;
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info) {
            if ($this->customer->isLogged()) {
                // Edit customers cart
                $this->load->model('account/wishlist');

                $this->model_account_wishlist->addWishlist($this->request->post['product_id']);

                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));

                $json['total'] = $this->model_account_wishlist->getTotalWishlist();
            } else {
                if (!isset($this->session->data['wishlist'])) {
                    $this->session->data['wishlist'] = array();
                }

                $this->session->data['wishlist'][] = $this->request->post['product_id'];

                $this->session->data['wishlist'] = array_unique($this->session->data['wishlist']);

                $json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));

//                $json['success'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', 'SSL'), $this->url->link('account/register', '', 'SSL'), $this->url->link('product/product', 'product_id=' . (int)$this->request->post['product_id']), $product_info['name'], $this->url->link('account/wishlist'));

                $json['total'] = isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
