<?php
class ControllerAccountPrice extends Controller {
	public function index() {

		$this->load->language('account/price');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('account/price', '', 'SSL')
		);

        $data['heading_title'] = $this->language->get('heading_title');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if ($this->customer->isLogged()) {//залогінений
            $this->load->model('account/download');
            $data['download_price'] = $this->language->get('download_price');
            $data['downloads'] = array();
            $category_ids = array(59,60,61);//Чоловічий/жіночий/дитячий асортимент
            $icons = array(
                59 => 'icon-man',
                60 => 'icon-woman',
                61 => 'icon-baby-boy'
            );//іконки для чоловічий/жіночий/дитячий асортимент

            foreach($category_ids as $category_id){
                $download_info = $this->model_account_download->getDownloadByCategory($category_id);
                $data['downloads'][] = array(
                    'download' => $this->url->link('account/price/download', 'download_id=' . $download_info['download_id'], 'SSL'),
                    'category_name' => $download_info['name'],
                    'icon' => $icons[$category_id]
                );
            }

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/price_login.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/price_login.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/account/price_login.tpl', $data));
            }
        }else{//незалогінений
            $data['text_un_login1'] = $this->language->get('text_un_login1');
            $data['text_un_login2'] = $this->language->get('text_un_login2');
            $data['text_authorization'] = $this->language->get('text_authorization');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/account/price_logged.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/account/price_logged.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/account/price_logged.tpl', $data));
            }
        }
	}

    public function download() {
        if (!$this->customer->isLogged()) {
            $this->session->data['redirect'] = $this->url->link('account/download', '', 'SSL');

            $this->response->redirect($this->url->link('account/login', '', 'SSL'));
        }

        $this->load->model('account/download');

        if (isset($this->request->get['download_id'])) {
            $download_id = $this->request->get['download_id'];
        } else {
            $download_id = 0;
        }

        $download_info = $this->model_account_download->getDownloadCategory($download_id);

        /*print_r($download_info);
        die();*/

        if ($download_info) {
            $file = DIR_DOWNLOAD . $download_info['filename'];
            $mask = basename($download_info['mask']);

            if (!headers_sent()) {
                if (file_exists($file)) {
                    header('Content-Type: application/octet-stream');
                    header('Content-Disposition: attachment; filename="' . ($mask ? $mask : basename($file)) . '"');
                    header('Expires: 0');
                    header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
                    header('Pragma: public');
                    header('Content-Length: ' . filesize($file));

                    if (ob_get_level()) {
                        ob_end_clean();
                    }

                    readfile($file, 'rb');

                    exit();
                } else {
                    exit('Error: Could not find file ' . $file . '!');
                }
            } else {
                exit('Error: Headers already sent out!');
            }
        } else {
            $this->response->redirect($this->url->link('account/download', '', 'SSL'));
        }
    }
}