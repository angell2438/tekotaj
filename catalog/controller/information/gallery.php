<?php
class ControllerInformationGallery extends Controller {
	public function index() {
		$this->load->language('information/gallery');

		$this->load->model('catalog/information');
		$this->load->model('tool/image');
		$this->load->model('design/banner');

        $data['text_pagin_next'] = $this->language->get('text_pagin_next');
        $data['text_pagin_prev'] = $this->language->get('text_pagin_prev');


		$banner_id = $this->config->get('config_banner'); //id баннера кторый будет использыватся для галереи

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        //количество фото на странице ставить кратное 5
        $limit = 10;

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

        $this->document->setTitle($this->language->get('heading_title'));
        $this->document->setDescription($this->language->get('heading_title'));
        $this->document->setKeywords($this->language->get('heading_title'));

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/gallery', '' , 'SSL')
        );

        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['text_empty'] = $this->language->get('text_empty');
        $data['continue'] = $this->url->link('common/home');

        $filter_data = array(
            'banner_id'      => $banner_id,
            'start'          => ($page - 1) * $limit,
            'limit'          => $limit
        );

        $photo_total = $this->model_design_banner->getCountPhotoFromBanner($filter_data);
        $results = $this->model_design_banner->getPhotoFromBanner($filter_data);

        $data['images'] = array();
        foreach ($results as $k => $result){
            if (($k == 0) || ($k == 1) || ($k == 5) || ($k == 6)){
                $size = 555;
                $height = 360;
            }else{
                $size = 360;
                $height = 235;
            }

            if($result['image']){
                $image = $this->model_tool_image->resize($result['image'], $size, $height);
            }else{
                $image = $this->model_tool_image->resize('no_image.png', $size, $height);
            }

            if($result['image']){
                $thumb = $this->model_tool_image->resize($result['image'], 750, 750);
            }else{
                $thumb = $this->model_tool_image->resize('no_image.png', 750, 750);
            }

            $data['images'][] = array(
                'title' => $result['title'],
                'image' => $image,
                'thumb' => $thumb,
            );
        }

        $pagination = new Paginationfront();
        $pagination->total = $photo_total;
        $pagination->page = $page;
        $pagination->limit = $limit;

        $pagination->text_prev = $data['text_pagin_prev'];
        $pagination->text_next = $data['text_pagin_next'];

        $pagination->url = $this->url->link('information/gallery', 'page={page}', 'SSL');

        $data['pagination'] = $pagination->render();
      //  var_dump();

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/gallery.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/gallery.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/information/gallery.tpl', $data));
        }

	}
}