<?php
class ControllerModuleCategory extends Controller {
	public function index() {
		$this->load->language('module/category');

		$data['heading_title'] = $this->language->get('heading_title');
        $data['text_watch'] = $this->language->get('text_watch');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $data['categories'] = array();

        $categories = $this->model_catalog_category->getCategories(0);

        foreach ($categories as $category) {
            if ($category['image']) {
                $image = $this->model_tool_image->resize($category['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
            } else {
                $image = $this->model_tool_image->resize('no_image.png', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
            }

            $filter_data = array(
                'filter_category_id'  => $category['category_id'],
                'filter_sub_category' => true
            );

            $data['categories'][] = array(
                'category_id' => $category['category_id'],
                'thumb'       => $image,
                'column'      => $category['column'] ? $category['column'] : 1,
                'name_blue'  => html_entity_decode(isset(explode(' ', $category['name'])[0])? (explode(' ', $category['name'])[0]): ''),
                'name_white'   => html_entity_decode(isset(explode(' ', $category['name'])[1])? (explode(' ', $category['name'])[1]): ''),
                'count'       => $this->config->get('config_product_count') ? $this->model_catalog_product->getTotalProducts($filter_data) : '',
                'href'        => $this->url->link('product/category', 'path=' . $category['category_id'])
            );
        }

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
		} else {
			return $this->load->view('default/template/module/category.tpl', $data);
		}
	}
}