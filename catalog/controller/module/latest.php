<?php
class ControllerModuleLatest extends Controller {
	public function index($setting) {
		$this->load->language('module/latest');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['by_one_click'] = $this->language->get('by_one_click');
        $data['text_timer'] = $this->language->get('text_timer');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');



		$data['products'] = array();

		$filter_data = array(
			'sort'  => 'p.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => $setting['limit']
		);



		$results = $this->model_catalog_product->getProducts($filter_data);

		if ($results) {
			foreach ($results as $result) {

//                list($year, $day, $minute) = explode('-', $result[0]['date_end']);
//
//                $start_time = strtotime($result['date_start']);
//                $end_time = strtotime($result['date_end']);
//                $time = $end_time - time();

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
				}

				if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}

                if ($result['news']) {
                    $news = $result['news'];
                } else {
                    $news = false;
                }


                if ($result['top']) {
                    $top = $result['top'];
                } else {
                    $top = false;
                }

                if (isset($this->session->data['wishlist']) && in_array($result['product_id'], $this->session->data['wishlist'])) {
                    $wishlist_status = '1';
                } else {
                    $wishlist_status = false;
                }

                if (isset($this->session->data['compare']) && in_array($result['product_id'], $this->session->data['compare'])) {
                    $compare_status = '1';
                } else {
                    $compare_status = false;
                }

                if ($result['sales'] AND isset($result['special'])) {
                    $sales = round(100-($result['special']*100/$result['price']),0);
                } else {
                    $sales = false;
                }

                if (!empty($result['special_end_date']) AND ($result['special_end_date']!='0000-00-00')) {
                    $special_end_date = explode('-', $result['special_end_date']);
                    $count_end_date =$special_end_date[1].', '.$special_end_date[2].', '.$special_end_date[0];
                    $count_end_date = $result['special_end_date'];
                } else
                    $count_end_date = false;

                $data['products'][] = array(
                    'product_id'  => $result['product_id'],
                    'thumb'       => $image,
                    'name'        => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price'       => $price,
                    'special'     => $special,
                    'tax'         => $tax,
                    'top'             => $top,
                    'count_end_date'  => $count_end_date,
                    'news'            => $news,
                    'sales'           => $sales,
                    'wishlist_status' => $wishlist_status,
                    'compare_status'  => $compare_status,
//					'time'        => $time,
					'rating'      => $rating,
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
//                var_dump($data['products']);
			}

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/latest.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/latest.tpl', $data);
			} else {
				return $this->load->view('default/template/module/latest.tpl', $data);
			}
		}
	}
}
