<?php
class ControllerModuleFeatured extends Controller {
	public function index($setting) {
		$this->load->language('module/featured');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['by_one_click'] = $this->language->get('by_one_click');
        $data['text_timer'] = $this->language->get('text_timer');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {

//                    list($year, $day, $minute) = explode('-', $product_info['date_end']);
//
//                    $start_time = strtotime($product_info['date_start']);
//                    $end_time = strtotime($product_info['date_end']);
//                    $time = $end_time - time();


					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}

					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}


                    if ($product_info['news']) {
                        $news = $product_info['news'];
                    } else {
                        $news = false;
                    }

                    if ($product_info['sales'] AND isset($product_info['special'])) {
                        $sales = round(100-($product_info['special']*100/$product_info['price']),0);
                    } else {
                        $sales = false;
                    }

                    if ($product_info['top']) {
                        $top = $product_info['top'];
                    } else {
                        $top = false;
                    }

                    if (isset($this->session->data['wishlist']) && in_array($product_info['product_id'], $this->session->data['wishlist'])) {
                        $wishlist_status = '1';
                    } else {
                        $wishlist_status = false;
                    }

                    if (isset($this->session->data['compare']) && in_array($product_info['product_id'], $this->session->data['compare'])) {
                        $compare_status = '1';
                    } else {
                        $compare_status = false;
                    }


                    if (!empty($product_info['special_end_date']) AND ($product_info['special_end_date']!='0000-00-00')) {
                        $special_end_date = explode('-', $product_info['special_end_date']);
                        $count_end_date =$special_end_date[1].', '.$special_end_date[2].', '.$special_end_date[0];
                        $count_end_date = $product_info['special_end_date'];
                    } else
                        $count_end_date = false;

//var_dump($product_info['product_id'].'-'.$count_end_date);
					$data['products'][] = array(
						'product_id'        => $product_info['product_id'],
						'thumb'             => $image,
						'name'              => $product_info['name'],
						'wishlist_status'   => $wishlist_status,
						'compare_status'    => $compare_status,
						'description'       => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
						'price'             => $price,
						'special'           => $special,
						'count_end_date'   => $count_end_date,
						'tax'               => $tax,
                        'news'              => $news,
                        'sales'             => $sales,
                        'top'               => $top,
//						'time'              => $time,
						'rating'            => $rating,
						'href'              => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
//					var_dump($data['products']);
				}
			}
		}

		if ($data['products']) {
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/featured.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/featured.tpl', $data);
			} else {
				return $this->load->view('default/template/module/featured.tpl', $data);
			}
		}
	}
}